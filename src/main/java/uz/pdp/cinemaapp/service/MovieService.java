package uz.pdp.cinemaapp.service;
//Sevinch Abdisattorova 03/14/2022 11:04 PM


import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.cinemaapp.dto.MovieDto;
import uz.pdp.cinemaapp.entity.*;
import uz.pdp.cinemaapp.entity.enums.MovieStatus;
import uz.pdp.cinemaapp.projection.CustomMovie;
import uz.pdp.cinemaapp.projection.CustomMovieById;
import uz.pdp.cinemaapp.repository.MovieRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class MovieService {


    private final MovieRepository movieRepository;
    private final AttachmentService attachmentService;
    private final DirectorService directorService;
    private final ActorService actorService;
    private final GenreService genreService;
    private final DistributorService distributorService;


    public Page<CustomMovie> getAllMovies(Integer page, Integer size, String search) {
        return movieRepository.getAllMovies(PageRequest.of(page - 1, size), search);
    }


    public CustomMovieById getMovieById(Integer id) {
        return movieRepository.getMovieById(id);
    }


    public boolean deleteMovie(Integer id) {
        try {
            movieRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    public boolean saveMovie(MovieDto movie) {
        try {
            movieRepository.save(new Movie(
                    movie.getTitle(),
                    movie.getDescription(),
                    movie.getDurationInMinutes(),
                    attachmentService.addAttachment(movie.getCoverImage()),
                    attachmentService.addAttachment(movie.getTrailerVideo()),
                    directorService.getDirectorsByIds(movie.getDirectorIds()),
                    actorService.getActorsByIds(movie.getActorIds()),
                    attachmentService.saveAttachments(movie.getPhotos()),
                    genreService.getGenresByIds(movie.getGenres()),
                    distributorService.getDistributorById(movie.getDistributorId()),
                    movie.getDistributorShareInPercent(),
                    movie.getMinPrice(),
                    MovieStatus.ACTIVE,
                    LocalDate.parse(movie.getReleaseDate(), DateTimeFormatter.ofPattern("yyyy:MM:dd")),
                    movie.getBudget()
            ));
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    public ResponseEntity<?> editMovie(Integer id, MovieDto movieDto) {
        Optional<Movie> byId = movieRepository.findById(id);
        if (byId.isPresent()) {
            Movie movie = byId.get();
            if (movieDto.getTitle() != null
                    && movieDto.getTitle().length() > 0) {
                movie.setTitle(movieDto.getTitle());
            }
            if (movieDto.getDescription() != null && movieDto.getDescription().length() > 0) {
                movie.setDescription(movieDto.getDescription());
            }
            if (movieDto.getDurationInMinutes() != null) {
                movie.setDurationInMinutes(movieDto.getDurationInMinutes());
            }
            if (movieDto.getDistributorId() != null) {
                distributorService.editDistributor(movie.getDistributor(), movieDto.getDistributorId());
            }
            if (movieDto.getDistributorShareInPercent() != null) {
                movie.setDistributorShareInPercent(movieDto.getDistributorShareInPercent());
            }
            if (movieDto.getMinPrice() != null) {
                movie.setMinPrice(movieDto.getMinPrice());
            }
            if (movieDto.getCoverImage() != null &&
                    Objects.requireNonNull(movieDto.getCoverImage().getOriginalFilename()).length() > 0) {

                Attachment attachment = attachmentService.
                        editAttachment(movie.getCoverImage().getId(),
                                movieDto.getCoverImage());
                movie.setCoverImage(attachment);
            }
            if (movieDto.getTrailerVideo() != null &&
                    Objects.requireNonNull(movieDto.getTrailerVideo()
                            .getOriginalFilename()).length() > 0) {
                Attachment attachment = attachmentService.
                        editAttachment(movie.getTrailerVideo().
                                getId(), movieDto.getTrailerVideo());
                movie.setTrailerVideo(attachment);
            }
            if (movieDto.getActorIds() != null) {
//                deleteActorsOfMovie(movie.getId());
                List<Actor> actorsByIds = actorService.getActorsByIds(movieDto.getActorIds());
                movie.setActors(actorsByIds);
            }
            if (movieDto.getDirectorIds() != null) {
//                directorRepository.deleteDirectorsOfMovie(movie.getId());
                List<Director> directorsByIds = directorService.getDirectorsByIds(movieDto.getDirectorIds());
                movie.setDirectors(directorsByIds);
            }
            if (movieDto.getGenres() != null) {
//                genreService.deleteGenresOfMovie(movie.getId());
                List<Genre> genresByIds = genreService.getGenresByIds(movieDto.getGenres());
                movie.setGenres(genresByIds);
            }
            if (movieDto.getPhotos() != null && movieDto.getPhotos().size() > 0) {
//                movieRepository.deletePhotosOfMovie(movie.getId());
                List<Attachment> attachments = attachmentService.saveAttachments(movieDto.getPhotos());
                movie.setPhotos(attachments);
            }
            if (movieDto.getBudget() != null) {
                movie.setBudget(movieDto.getBudget());
            }
            if (movieDto.getReleaseDate() != null) {
                movie.setReleaseDate(LocalDate.parse(movieDto.getReleaseDate(), DateTimeFormatter.ofPattern("yyyy:MM:dd")));
            }
            Movie savedMovie = movieRepository.save(movie);
            return ResponseEntity.ok(savedMovie);
        }
        return ResponseEntity.notFound().build();
    }


}
