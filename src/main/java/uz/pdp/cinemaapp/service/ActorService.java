package uz.pdp.cinemaapp.service;
//Sevinch Abdisattorova 03/14/2022 11:04 PM


import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.cinemaapp.entity.Actor;
import uz.pdp.cinemaapp.entity.Attachment;
import uz.pdp.cinemaapp.repository.ActorRepository;
import uz.pdp.cinemaapp.repository.AttachmentContentRepository;
import uz.pdp.cinemaapp.repository.AttachmentRepository;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class ActorService {

    private final ActorRepository actorRepository;
    private final AttachmentService attachmentService;

    public List<Actor> getAllActors() {
        return actorRepository.findAll();
    }


    public Actor getActorById(Integer id) {
        Optional<Actor> optionalActor = actorRepository.findById(id);
        return optionalActor.orElse(null);
    }


    public boolean deleteActor(Integer id) {
        try {
            actorRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    public boolean saveActor(Actor actor) {

        Attachment attachment = attachmentService.addAttachment(actor.getImage());
//        attachmentContentService.addAttachmentContent(file, attachment);
        actor.setPhoto(attachment);
        Actor saved = actorRepository.save(actor);
        return saved != null;
    }


    public Actor editActor(Integer id, Actor actor) {

        Optional<Actor> byId = actorRepository.findById(id);
        if (byId.isPresent()) {
            Actor actor1 = byId.get();
            Attachment editingPhoto = actor1.getPhoto();
            if (actor.getFullName() != null) {
                actor1.setFullName(actor.getFullName());
            }
            if (actor.getImage() != null && Objects.requireNonNull(actor.getImage()
                    .getOriginalFilename()).length() > 1) {
                Attachment attachment = attachmentService.
                        editAttachment(editingPhoto.getId(), actor.getImage());

                actor1.setPhoto(editingPhoto);
            }
            actorRepository.save(actor1);
            return actor1;
        }
        return null;
    }


    public List<Actor> getActorsByIds(List<Integer> ids) {
        return actorRepository.findAllById(ids);
    }


    public boolean deleteActors(List<Actor> actors) {
        try {
            actorRepository.deleteAll(actors);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}

