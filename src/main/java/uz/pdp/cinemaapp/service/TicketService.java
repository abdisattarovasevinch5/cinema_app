package uz.pdp.cinemaapp.service;
//Sevinch Abdisattorova 03/31/2022 10:47 AM


import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.cinemaapp.entity.Attachment;
import uz.pdp.cinemaapp.entity.AttachmentContent;
import uz.pdp.cinemaapp.entity.CashBox;
import uz.pdp.cinemaapp.entity.Ticket;
import uz.pdp.cinemaapp.entity.enums.TicketStatus;
import uz.pdp.cinemaapp.repository.AttachmentContentRepository;
import uz.pdp.cinemaapp.repository.AttachmentRepository;
import uz.pdp.cinemaapp.repository.CashBoxRepository;
import uz.pdp.cinemaapp.repository.TicketRepository;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class TicketService {

    private final TicketRepository ticketRepository;
    private final AttachmentContentRepository attachmentContentRepository;
    private final AttachmentRepository attachmentRepository;
    private final CashBoxRepository cashBoxRepository;


    public void changeTicketToPurchasedAndGenerateQrCode(Integer userId) {
        List<Ticket> tickets = ticketRepository.findByUserIdAndStatus(userId, TicketStatus.NEW);
        double price = 0;
        for (Ticket ticket : tickets) {
            price += ticket.getPrice();
            ticket.setStatus(TicketStatus.PURCHASED);
            Attachment qrCodeAttachment = getQrCodeAttachment(ticket.getId());
            ticket.setQrCode(qrCodeAttachment);
        }
        ticketRepository.saveAll(tickets);
        CashBox cashBox = cashBoxRepository.findById(1).get();
        cashBox.setBalance(cashBox.getBalance() + price);

    }


    public byte[] getQRCodeImage(String text, int width, int height)
            throws WriterException, IOException {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);

        ByteArrayOutputStream pngOutputStream = new ByteArrayOutputStream();
        MatrixToImageWriter.writeToStream(bitMatrix, "PNG", pngOutputStream);
        byte[] pngData = pngOutputStream.toByteArray();
        return pngData;
    }


    public Attachment getQrCodeAttachment(Integer ticketId) {
        Optional<Ticket> byId = ticketRepository.findById(ticketId);
        if (byId.isPresent()) {
            Ticket ticket = byId.get();
            try {
                byte[] qrCodeImage = new byte[0];
                qrCodeImage = getQRCodeImage(ticket.getId().toString(), 200, 200);
                Attachment attachment = new Attachment();
                attachment.setName(ticket.getId().toString());
                attachment.setContentType("image/png");
                AttachmentContent attachmentContent = new AttachmentContent(qrCodeImage, attachment);
                attachmentContentRepository.save(attachmentContent);
                attachmentRepository.save(attachment);
                return attachment;
            } catch (WriterException | IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


}
