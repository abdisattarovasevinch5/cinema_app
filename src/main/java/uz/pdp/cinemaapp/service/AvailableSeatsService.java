package uz.pdp.cinemaapp.service;
//Sevinch Abdisattorova 03/25/2022 8:20 AM

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.cinemaapp.projection.CustomSeat;
import uz.pdp.cinemaapp.repository.SeatRepository;

import java.util.List;

@RequiredArgsConstructor
@Service
public class AvailableSeatsService {

    private final SeatRepository seatRepository;

    public ResponseEntity<?> getAllAvailableSeats(Integer movieSessionId) {
        List<CustomSeat> availableSeatsOfMovieSession = seatRepository
                .findAllAvailableSeatsOfMovieSession(movieSessionId);
        return ResponseEntity.ok(availableSeatsOfMovieSession);
    }
}
