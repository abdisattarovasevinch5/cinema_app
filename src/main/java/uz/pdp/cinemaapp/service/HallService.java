package uz.pdp.cinemaapp.service;
//Sevinch Abdisattorova 03/17/2022 4:39 PM


import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.cinemaapp.dto.HallDto;
import uz.pdp.cinemaapp.entity.Hall;
import uz.pdp.cinemaapp.entity.PriceCategory;
import uz.pdp.cinemaapp.entity.Row;
import uz.pdp.cinemaapp.entity.Seat;
import uz.pdp.cinemaapp.projection.CustomHallById;
import uz.pdp.cinemaapp.projection.CustomHallWithVIPFee;
import uz.pdp.cinemaapp.projection.CustomRowSeats;
import uz.pdp.cinemaapp.repository.HallRepository;
import uz.pdp.cinemaapp.repository.PriceCategoryRepository;
import uz.pdp.cinemaapp.repository.RowRepository;
import uz.pdp.cinemaapp.repository.SeatRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class HallService {

    private final HallRepository hallRepository;
    private final RowRepository rowRepository;
    private final SeatRepository seatRepository;
    private final PriceCategoryRepository priceCategoryRepository;

    public List<CustomHallWithVIPFee> getAllHalls() {
        return hallRepository.getAllHalls();
    }

    public CustomHallById getHallById(Integer id) {
        CustomHallById hallById = hallRepository.findHallById(id);
        return hallById;
    }

    public ResponseEntity<?> addHall(HallDto hallDto) {
        Hall hall = new Hall();
        hall.setName(hallDto.getName());
        hall.setVipAdditionalFeeInPercent(hallDto.getVipAdditionalFeeInPercent());

        List<Row> rows = new ArrayList<>();

        List<CustomRowSeats> customRowSeats = hallDto.getRows();
        for (CustomRowSeats customRowSeat : customRowSeats) {
            Integer rowNumber = customRowSeat.getRowNumber();
            Integer seatsCount = customRowSeat.getSeatsCount();
            Row row = new Row();
            row.setHall(hall);
            row.setNumber(rowNumber);
            PriceCategory priceCategory = priceCategoryRepository
                    .findById(customRowSeat.getPriceCategoryId()).get();
            List<Seat> seats = new ArrayList<>();
            for (int i = 0; i < seatsCount; i++) {
                Seat seat = new Seat(i + 1, row, priceCategory);
                seats.add(seat);
            }
            row.setSeats(seats);
            rows.add(row);
        }

        hall.setRows(rows);
        hallRepository.save(hall);
        return new ResponseEntity<>("Successfully created!", HttpStatus.CREATED);
    }

    public boolean editHall(HallDto hallDto, Integer id) {
        Optional<Hall> byId = hallRepository.findById(id);
        if (byId.isPresent()) {
            Hall hall = byId.get();
            if (hallDto.getName() != null) {
                hall.setName(hallDto.getName());
            }
            if (hallDto.getVipAdditionalFeeInPercent() != null) {
                hall.setVipAdditionalFeeInPercent(hallDto.getVipAdditionalFeeInPercent());
            }
            hallRepository.save(hall);
            return true;
        }
        return false;
    }

    public boolean deleteHallById(Integer id) {
        try {
            hallRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
