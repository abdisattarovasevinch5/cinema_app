package uz.pdp.cinemaapp.service;
//Sevinch Abdisattorova 03/15/2022 11:52 AM

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.cinemaapp.entity.Attachment;
import uz.pdp.cinemaapp.entity.AttachmentContent;
import uz.pdp.cinemaapp.repository.AttachmentContentRepository;
import uz.pdp.cinemaapp.repository.AttachmentRepository;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service

public class AttachmentContentService {
    private final AttachmentContentRepository attachmentContentRepository;
    private final AttachmentRepository attachmentRepository;


    public List<AttachmentContent> attachments() {
        List<AttachmentContent> all = attachmentContentRepository.findAll();
        return all;
    }


    public AttachmentContent addAttachmentContent(MultipartFile file, Attachment attachment) {
        try {
            AttachmentContent attachmentContent = attachmentContentRepository.save(new AttachmentContent(
                    file.getBytes(),
                    attachment
            ));
            return attachmentContent;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }


    public AttachmentContent editAttachmentContent(Integer editingAttachmentId,
                                                   Attachment attachment,
                                                   MultipartFile file) {
        AttachmentContent attachmentCont = attachmentContentRepository.findAttachmentContentByAttachment_Id(editingAttachmentId);
        try {
            attachmentCont.setAttachment(attachment);
            attachmentCont.setData(file.getBytes());
            return attachmentContentRepository.save(attachmentCont);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public AttachmentContent getAttachmentContentByAttachmentId(Integer id) {
        AttachmentContent attachmentContent =
                attachmentContentRepository.findAttachmentContentByAttachment_Id(id);
        return attachmentContent;
    }

    public byte[] getAttachmentContentId(Integer id) {

        byte[] attachmentContentData = attachmentContentRepository.findAttachmentContentByAttachmentId(id);
        return attachmentContentData;
    }


    public void getFile(Integer id, HttpServletResponse response) {
        Optional<Attachment> optionalAttachment = attachmentRepository.findById(id);
        if (optionalAttachment.isPresent()) {
            Attachment attachment = optionalAttachment.get();
            AttachmentContent attachmentContentFromDb = attachmentContentRepository.findAttachmentContentByAttachment_Id(id);
            if (attachmentContentFromDb != null) {
                response.setHeader("Content-Disposition", "attachment; filename=\"" + attachment.getName() + "\"");

                response.setContentType(attachment.getContentType());

                try {
                    FileCopyUtils.copy(attachmentContentFromDb.getData(), response.getOutputStream());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
