package uz.pdp.cinemaapp.service;
//Sevinch Abdisattorova 03/27/2022 12:53 PM


import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Refund;
import com.stripe.param.RefundCreateParams;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.cinemaapp.entity.CashBox;
import uz.pdp.cinemaapp.entity.Ticket;
import uz.pdp.cinemaapp.entity.enums.TicketStatus;
import uz.pdp.cinemaapp.projection.CustomTicket;
import uz.pdp.cinemaapp.repository.CashBoxRepository;
import uz.pdp.cinemaapp.repository.TicketRepository;
import uz.pdp.cinemaapp.repository.TransactionHistoryRepository;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class PaymentService {

    @Value("${STRIPE_SECRET_KEY}")
    String stripeApiKey;


    private final CashBoxRepository cashBoxRepository;
    private final TicketRepository ticketRepository;
    private final TransactionHistoryService transactionHistoryService;
    private final StripePaymentService stripePaymentService;
    private final TransactionHistoryRepository transactionHistoryRepository;


    public ResponseEntity<?> purchaseTicket(Integer userId, Integer payTypeId) {
        List<CustomTicket> ticketsInTheCart = ticketRepository.getTicketsInTheCart(userId);
        try {
            return stripePaymentService.createStripeSession(ticketsInTheCart, userId);
        } catch (StripeException e) {
            e.printStackTrace();
        }
        return ResponseEntity.badRequest().build();
    }


    public ResponseEntity<?> refundTickets(List<Integer> ticketIds) {

        Stripe.apiKey = stripeApiKey;
        String paymentIntent = transactionHistoryRepository
                .getPaymentIntentByTicketId(ticketIds.get(0));

        List<Ticket> tickets = new ArrayList<>();
        double ticketsPrice = 0;

        for (Integer ticketId : ticketIds) {
            Ticket ticket = ticketRepository.findById(ticketId).get();
            ticket.setStatus(TicketStatus.REFUNDED);
            Timestamp startTimeOfTicket = ticketRepository.getStartTimeOfTicket(ticketId);
            Integer percent = ticketRepository.getRefundChargeFeePercentByTicketId(startTimeOfTicket);
            Double price = ticket.getPrice();
            ticketsPrice += price - price * percent / 100;
            tickets.add(ticket);
        }
        long amount = (long) Math.ceil(ticketsPrice * 100);

        RefundCreateParams params = RefundCreateParams
                .builder()
                .setPaymentIntent(paymentIntent)
                .setAmount(amount)
                .build();

        try {
            Refund refund = Refund.create(params);
            if (refund.getStatus().equals("succeeded")) {
                CashBox cashBox = cashBoxRepository.findById(1).get();
                cashBox.setBalance(cashBox.getBalance() + ticketsPrice);
                cashBoxRepository.save(cashBox);
                transactionHistoryService.saveTransactionHistory(tickets,
                        1,
                        amount,
                        null,
                        true);

            }

        } catch (StripeException e) {
            e.printStackTrace();
        }
        return null;

    }
}
