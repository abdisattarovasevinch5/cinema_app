package uz.pdp.cinemaapp.service;
//Sevinch Abdisattorova 03/18/2022 11:04 AM


import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.cinemaapp.projection.CustomMovieSession;
import uz.pdp.cinemaapp.repository.MovieSessionRepository;

@RequiredArgsConstructor
@Service
public class MovieSessionService {

    private final MovieSessionRepository movieSessionRepository;

    public ResponseEntity<?> getAllMovieSessions(Integer page, Integer size, String search) {

        Pageable pageable = PageRequest.of(page - 1, size);

        Page<CustomMovieSession> allSessionsByPage =
                movieSessionRepository.findAllSessionsByPage(pageable, search);

        return ResponseEntity.ok(allSessionsByPage);
    }
}
