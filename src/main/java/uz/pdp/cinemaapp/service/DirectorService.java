package uz.pdp.cinemaapp.service;
//Sevinch Abdisattorova 03/15/2022 11:47 PM

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.pdp.cinemaapp.entity.Director;
import uz.pdp.cinemaapp.repository.DirectorRepository;

import java.util.List;
import java.util.Optional;


@RequiredArgsConstructor
@Service
public class DirectorService {

    private final DirectorRepository directorRepository;

    public List<Director> getDirectorsByIds(List<Integer> ids) {
        return directorRepository.findAllById(ids);
    }


    public List<Director> getAllDirectors() {
        return directorRepository.findAll();
    }


    public Director getDirectorById(Integer id) {
        Optional<Director> byId = directorRepository.findById(id);
        return byId.orElse(null);
    }


    public boolean addDirector(Director director) {
        Director save = directorRepository.save(director);
        return save != null;
    }


    public boolean editDirector(Director director, Integer id) {
        Optional<Director> byId = directorRepository.findById(id);
        if (byId.isPresent()) {
            Director director1 = byId.get();
            if (director1.getFullName() != null) {
                director1.setFullName(director.getFullName());
            }
            directorRepository.save(director1);
            return true;
        }
        return false;
    }


    public boolean deleteDirectorById(Integer id) {
        try {
            directorRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
