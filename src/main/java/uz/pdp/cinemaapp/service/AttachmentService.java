package uz.pdp.cinemaapp.service;
//Sevinch Abdisattorova 03/15/2022 11:52 AM

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.cinemaapp.entity.Attachment;
import uz.pdp.cinemaapp.entity.AttachmentContent;
import uz.pdp.cinemaapp.repository.AttachmentContentRepository;
import uz.pdp.cinemaapp.repository.AttachmentRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class AttachmentService {

    private final AttachmentRepository attachmentRepository;


    private final AttachmentContentRepository attachmentContentRepository;

    private final AttachmentContentService attachmentContentService;

    public List<Attachment> attachments() {
        List<Attachment> all = attachmentRepository.findAll();
        return all;
    }

    public Attachment addAttachment(MultipartFile file) {
        Attachment attachment =
                new Attachment(file.getOriginalFilename(),
                        file.getSize(), file.getContentType());
        attachmentContentService.addAttachmentContent(file, attachment);
        return attachmentRepository.save(attachment);
    }


    public Attachment editAttachment(Integer editingAttachmentId, MultipartFile file) {
        Attachment attachment = getAttachmentById(editingAttachmentId);
        attachment.setContentType(file.getContentType());
        attachment.setSize(file.getSize());
        attachment.setName(file.getOriginalFilename());

        AttachmentContent attachmentContent = attachmentContentService.
                editAttachmentContent(editingAttachmentId, attachment, file);

        return attachmentRepository.save(attachment);
    }


    public boolean deleteAttachmentById(Integer id) {
        try {
            attachmentRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Attachment getAttachmentById(Integer id) {
        Optional<Attachment> byId = attachmentRepository.findById(id);
        return byId.orElse(null);
    }

    public List<Attachment> saveAttachments(List<MultipartFile> photos) {
        List<Attachment> attachments = new ArrayList<>();
        for (MultipartFile photo : photos) {
            Attachment attachment = new Attachment(photo.getContentType(),
                    photo.getSize(), photo.getOriginalFilename());
            attachments.add(
                    attachment
            );
            attachmentContentService.addAttachmentContent(photo, attachment);
        }

        return attachments;
    }
}
