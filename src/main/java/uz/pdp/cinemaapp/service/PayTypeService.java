package uz.pdp.cinemaapp.service;
//Sevinch Abdisattorova 03/28/2022 1:49 AM


import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.cinemaapp.entity.PayType;
import uz.pdp.cinemaapp.repository.PaymentTypeRepository;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class PayTypeService {

    private final PaymentTypeRepository paymentTypeRepository;


    public List<PayType> getAllPayTypes() {
        return paymentTypeRepository.findAll();
    }


    public PayType getPayTypeById(Integer id) {
        Optional<PayType> byId = paymentTypeRepository.findById(id);
        return byId.orElse(null);

    }


    public ResponseEntity<?> addPayType(PayType payType) {
        PayType save = paymentTypeRepository.save(payType);
        if (save != null) {
            return new ResponseEntity("Successfully created", HttpStatus.CREATED);
        } else return new ResponseEntity("Conflict", HttpStatus.CONFLICT);
    }


    public boolean editPayType(PayType payType, Integer id) {
        Optional<PayType> byId = paymentTypeRepository.findById(id);
        if (byId.isPresent()) {
            PayType payType1 = byId.get();
            if (payType.getCommissionFeeInPercent() != null) {
                payType1.setCommissionFeeInPercent(payType.getCommissionFeeInPercent());
            }
            if (payType.getName() != null) {
                payType1.setName(payType.getName());
            }
            paymentTypeRepository.save(payType1);
            return true;
        }
        return false;
    }


    public boolean deletePayTypeById(Integer id) {
        try {
            paymentTypeRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }
}
