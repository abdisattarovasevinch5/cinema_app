package uz.pdp.cinemaapp.service;
//Sevinch Abdisattorova 03/17/2022 10:34 AM

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.cinemaapp.dto.MovieAnnouncementDto;
import uz.pdp.cinemaapp.entity.Movie;
import uz.pdp.cinemaapp.entity.MovieAnnouncement;
import uz.pdp.cinemaapp.projection.CustomMovieAnnouncement;
import uz.pdp.cinemaapp.repository.MovieAnnouncementRepository;
import uz.pdp.cinemaapp.repository.MovieRepository;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class MovieAnnouncementService {


    private final MovieAnnouncementRepository movieAnnouncementRepository;

    private final MovieRepository movieRepository;


   /* public ResponseEntity getAllMovieAnnouncements(Integer page,
                                                   Integer size,
                                                   String search,
                                                   String sort,
                                                   boolean direction) {
        Pageable pageable = PageRequest.of(page - 1,
                size, direction ? Sort.Direction.ASC : Sort.Direction.DESC,
                sort);

        List<CustomMovieAnnouncement> allMovieAnnouncement = movieAnnouncementRepository.getAllMovieAnnouncements(pageable, search);
        return ResponseEntity.ok(allMovieAnnouncement);
    }*/

    public ResponseEntity<?> makeMovieAnnouncement(MovieAnnouncementDto movieAnnouncementDto) {
        MovieAnnouncement movieAnnouncementByMovieId = movieAnnouncementRepository.
                findMovieAnnouncementByMovieId(movieAnnouncementDto.getMovieId());
        if (movieAnnouncementByMovieId == null) {
            Optional<Movie> byId = movieRepository.findById(movieAnnouncementDto.getMovieId());
            if (byId.isPresent()) {
                MovieAnnouncement movieAnnouncement = new MovieAnnouncement(byId.get(), movieAnnouncementDto.getIsActive() != null ? movieAnnouncementDto.getIsActive() : false);
                movieAnnouncementRepository.save(movieAnnouncement);
                return new ResponseEntity("Successfully saved!", HttpStatus.CREATED);
            }
            return new ResponseEntity("Movie not found", HttpStatus.NOT_FOUND);
        }
        if (movieAnnouncementDto.getMovieId() != null) {
            Optional<Movie> byId1 = movieRepository.findById(movieAnnouncementDto.getMovieId());
            if (!byId1.isPresent())
                return new ResponseEntity("Movie not found", HttpStatus.NOT_FOUND);
            movieAnnouncementByMovieId.setMovie(byId1.get());
        }
        if (movieAnnouncementDto.getIsActive() != null) {
            movieAnnouncementByMovieId.setActive(movieAnnouncementDto.getIsActive());
        }
        movieAnnouncementRepository.save(movieAnnouncementByMovieId);
        return new ResponseEntity("Successfully updated!", HttpStatus.OK);
    }


    public ResponseEntity<?> editMovieAnnouncement(MovieAnnouncementDto movieAnnouncementDto, Integer afishaId) {

        Optional<MovieAnnouncement> byId = movieAnnouncementRepository.findById(afishaId);
        if (byId.isPresent()) {
            MovieAnnouncement movieAnnouncement = byId.get();
            if (movieAnnouncementDto.getMovieId() != null) {
                Optional<Movie> byId1 = movieRepository.findById(movieAnnouncementDto.getMovieId());
                if (!byId1.isPresent())
                    return new ResponseEntity("Movie not found", HttpStatus.NOT_FOUND);
                movieAnnouncement.setMovie(byId1.get());
            }
            if (movieAnnouncementDto.getIsActive() != null) {
                movieAnnouncement.setActive(movieAnnouncementDto.getIsActive());
            }
            movieAnnouncementRepository.save(movieAnnouncement);
            return new ResponseEntity("Successfully updated!", HttpStatus.OK);
        }
        return new ResponseEntity("MovieAnnouncement not found!", HttpStatus.NOT_FOUND);
    }


    public ResponseEntity<?> deleteMovieAnnouncement(Integer afishaId) {
        try {
            movieAnnouncementRepository.deleteById(afishaId);
            return ResponseEntity.noContent().build();
        } catch (Exception ignored) {
        }
        return ResponseEntity.notFound().build();
    }


    public ResponseEntity<?> getAllMovieAnnouncements() {
        List<CustomMovieAnnouncement> allMovieAnnouncements =
                movieAnnouncementRepository.getAllMovieAnnouncements();
        return ResponseEntity.ok(allMovieAnnouncements);
    }


    public ResponseEntity<?> getMovieAnnouncementById(Integer movieAnnouncementId) {
        CustomMovieAnnouncement movieAnnouncementById = movieAnnouncementRepository.getMovieAnnouncementById(movieAnnouncementId);
        if (movieAnnouncementById != null) {
            return ResponseEntity.ok(movieAnnouncementById);
        } else return new ResponseEntity("Movie announcement not found", HttpStatus.NOT_FOUND);
    }
}
