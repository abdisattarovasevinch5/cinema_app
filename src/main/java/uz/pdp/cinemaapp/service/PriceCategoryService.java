package uz.pdp.cinemaapp.service;
//Sevinch Abdisattorova 03/24/2022 6:01 PM

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.cinemaapp.entity.PriceCategory;
import uz.pdp.cinemaapp.repository.PriceCategoryRepository;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class PriceCategoryService {


    private final PriceCategoryRepository priceCategoryRepository;


    public ResponseEntity<?> getAllPriceCategories() {
        List<PriceCategory> priceCategories = priceCategoryRepository.findAll();
        if (priceCategories.size() > 0) {
            return ResponseEntity.ok(priceCategories);
        } else return new ResponseEntity("No price categories yet", HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<?> getPriceCategoryById(Integer id) {
        Optional<PriceCategory> byId = priceCategoryRepository.findById(id);
        if (byId.isPresent()) {
            return ResponseEntity.ok(byId.get());
        } else return ResponseEntity.notFound().build();
    }

    public ResponseEntity<?> addPriceCategory(PriceCategory priceCategory) {
        PriceCategory save = priceCategoryRepository.save(priceCategory);
        if (save != null) {
            return new ResponseEntity(save, HttpStatus.CREATED);
        } else
            return ResponseEntity.badRequest().build();
    }

    public ResponseEntity<?> editPriceCategory(Integer id, PriceCategory priceCategory) {
        Optional<PriceCategory> byId = priceCategoryRepository.findById(id);
        if (byId != null) {
            PriceCategory priceCategoryById = byId.get();
            if (priceCategory.getName() != null) {
                priceCategoryById.setName(priceCategory.getName());
            }
            if (priceCategory.getColor() != null) {
                priceCategoryById.setColor(priceCategory.getColor());
            }
            if (priceCategory.getAdditionalFeeInPercent() != null) {
                priceCategoryById.setAdditionalFeeInPercent(priceCategory.getAdditionalFeeInPercent());
            }
            priceCategoryRepository.save(priceCategoryById);
            return ResponseEntity.ok("Successfully updated!");
        }
        return new ResponseEntity("Price category with id " + id + " not found", HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<?> deletePriceCategoryById(Integer id) {
        try {
            priceCategoryRepository.deleteById(id);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
}
