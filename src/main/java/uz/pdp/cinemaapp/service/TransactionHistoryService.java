package uz.pdp.cinemaapp.service;
//Sevinch Abdisattorova 03/27/2022 12:53 PM


import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.cinemaapp.entity.PayType;
import uz.pdp.cinemaapp.entity.TransactionHistory;
import uz.pdp.cinemaapp.entity.Ticket;
import uz.pdp.cinemaapp.entity.enums.TicketStatus;
import uz.pdp.cinemaapp.projection.CustomTransactionHistory;
import uz.pdp.cinemaapp.projection.CustomTransactionHistoryForUser;
import uz.pdp.cinemaapp.repository.PaymentTypeRepository;
import uz.pdp.cinemaapp.repository.TransactionHistoryRepository;
import uz.pdp.cinemaapp.repository.TicketRepository;

import java.util.List;

@RequiredArgsConstructor
@Service
public class TransactionHistoryService {

    private final TransactionHistoryRepository transactionHistoryRepository;
    private final PaymentTypeRepository paymentTypeRepository;


    public void saveTransactionHistory(List<Ticket> tickets, Integer paymentTypeId, double amountTotal, String paymentIntent, boolean isRefunded) {
        try {
            PayType payType = paymentTypeRepository.getById(paymentTypeId);
            TransactionHistory transactionHistory = new TransactionHistory(tickets, amountTotal, payType, paymentIntent, isRefunded);
            transactionHistoryRepository.save(transactionHistory);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public ResponseEntity<?> getAllTransactionHistories() {
        List<CustomTransactionHistory> all = transactionHistoryRepository.getAllTransactionHistories();
        return ResponseEntity.ok(all);
    }


    public ResponseEntity<?> getAllTransactionHistoriesOfUser(Integer userId) {
        List<CustomTransactionHistoryForUser> allTransactionHistoriesOfUser = transactionHistoryRepository.getAllTransactionHistoriesOfUser(userId);
        if (allTransactionHistoriesOfUser.size() > 0) {
            return ResponseEntity.ok(allTransactionHistoriesOfUser);
        } else
            return new ResponseEntity("No transactions yet", HttpStatus.NOT_FOUND);
    }
}
