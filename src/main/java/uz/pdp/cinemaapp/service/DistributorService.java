package uz.pdp.cinemaapp.service;
//Sevinch Abdisattorova 03/16/2022 12:23 AM

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.pdp.cinemaapp.entity.Distributor;
import uz.pdp.cinemaapp.repository.DistributorRepository;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class DistributorService {

    private final DistributorRepository distributorRepository;

    public Distributor getDistributorById(Integer id) {
        return distributorRepository.findById(id).get();
    }

    public List<Distributor> getAllDistributors() {
        return distributorRepository.findAll();
    }

    public boolean addDistributor(Distributor distributor) {
        Distributor save = distributorRepository.save(distributor);
        return save != null;
    }

    public boolean editDistributor(Distributor distributorDto, Integer id) {
        Optional<Distributor> byId = distributorRepository.findById(id);
        if (byId.isPresent()) {
            Distributor distributor1 = byId.get();
            if (distributorDto.getName() != null) {
                distributor1.setName(distributorDto.getName());
            }
            if (distributorDto.getDescription() != null) {
                distributor1.setDescription(distributorDto.getDescription());
            }
            distributorRepository.save(distributor1);
            return true;
        }
        return false;
    }

    public boolean deleteDistributorById(Integer id) {
        try {
            distributorRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
