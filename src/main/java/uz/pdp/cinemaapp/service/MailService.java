package uz.pdp.cinemaapp.service;

import com.stripe.model.checkout.Session;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import uz.pdp.cinemaapp.entity.User;
import uz.pdp.cinemaapp.projection.CustomTicket;
import uz.pdp.cinemaapp.projection.CustomTicketForEmail;
import uz.pdp.cinemaapp.repository.TicketRepository;
import uz.pdp.cinemaapp.repository.UserRepository;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RequiredArgsConstructor
@Service("mailService")
public class MailService {


    @Value("${PERSONAL}")
    String personal;

    private final JavaMailSender javaMailSender;

    private final TicketRepository ticketRepository;

    private final UserRepository userRepository;

    private final TemplateEngine templateEngine;

    public void sendEmailToClient(Session session, Integer userId) {
        List<CustomTicketForEmail> tickets = ticketRepository.getTicketsInTheCartForPurchaseEmail(userId);

        double totalPrice = 0;
        for (CustomTicketForEmail ticket : tickets) {
            totalPrice += ticket.getPrice();
        }
        User user = userRepository.findById(userId).get();
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(message,
                    MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());
            Context context = new Context();
            Map<String, Object> props = new HashMap<>();

            props.put("fullName", user.getFullName());
            props.put("date", LocalDateTime.now().
                    format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
            props.put("time", LocalDateTime.now().
                    format(DateTimeFormatter.ofPattern("HH:mm")));
            props.put("tickets", tickets);
            props.put("totalPrice", totalPrice);
            context.setVariables(props);
            helper.setFrom("abdisattarovasevinch5@gmail.com");
            helper.setTo(session.getCustomerDetails().getEmail());
            helper.setSubject("Purchase info");
            String html = templateEngine.process("email.html", context);
            helper.setText(html, true);

            javaMailSender.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
