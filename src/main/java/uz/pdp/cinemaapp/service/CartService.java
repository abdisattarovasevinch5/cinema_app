package uz.pdp.cinemaapp.service;
//Sevinch Abdisattorova 03/25/2022 9:21 AM

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.cinemaapp.entity.MovieSession;
import uz.pdp.cinemaapp.entity.Seat;
import uz.pdp.cinemaapp.entity.Ticket;
import uz.pdp.cinemaapp.entity.User;
import uz.pdp.cinemaapp.entity.enums.TicketStatus;
import uz.pdp.cinemaapp.projection.CustomTicket;
import uz.pdp.cinemaapp.projection.CustomTicketForCart;
import uz.pdp.cinemaapp.repository.*;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

@RequiredArgsConstructor
@Service
public class CartService {

    private final TicketRepository ticketRepository;

    private final UserRepository userRepository;

    private final SeatRepository seatRepository;

    private final MovieSessionRepository movieSessionRepository;

    private final WaitingTimeRepository waitingTimeRepository;

    public ResponseEntity<?> addToCart(Integer userId, Integer seatId, Integer movieSessionId) {

        CustomTicketForCart customTicket = ticketRepository.checkIfSeatIsAvailable(seatId, movieSessionId);
        if (customTicket != null) {
            return new ResponseEntity("This seat is not available", HttpStatus.CONFLICT);
        }

        User user = userRepository.findById(userId).get();
        Seat seat = seatRepository.findById(seatId).get();
        MovieSession movieSession = movieSessionRepository.findById(movieSessionId).get();
        Ticket ticket = new Ticket(movieSession, seat, null, getPriceOfSeat(seatId, movieSessionId), TicketStatus.NEW, user);
        ticketRepository.save(ticket);
        Integer waitingMinute = waitingTimeRepository.getWaitingMinute();
        if (ticket != null) {
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    CustomTicketForCart ticketByIdForCart = ticketRepository.getTicketByIdForCart(ticket.getId());
                    try {
                        if (ticketByIdForCart.getStatus().equals(TicketStatus.NEW)) {
                            ticketRepository.deleteById(ticket.getId());
                            System.out.println("Ticket is deleted " + ticket.getId());
                        }
                    } catch (NullPointerException ignored) {
                    }
                }
            };
            Timer timer = new Timer();
            System.out.println("after " + waitingMinute + " minutes ticket will be deleted!");
            timer.schedule(timerTask, waitingMinute * 60000);
            return new ResponseEntity("Successfully added to cart", HttpStatus.CREATED);
        } else
            return ResponseEntity.badRequest().build();

    }


    private Double getPriceOfSeat(Integer seatId, Integer movieSessionId) {
        return seatRepository.getPriceOfSeatBySeatIdAndMovieSessionId(seatId, movieSessionId);
    }


    public ResponseEntity<?> getTicketsInTheCart(Integer userId) {
        List<CustomTicket> tickets = ticketRepository.getTicketsInTheCart(userId);
        if (tickets != null && tickets.size() > 0) {
            return ResponseEntity.ok(tickets);
        } else return new ResponseEntity("No tickets yet", HttpStatus.NOT_FOUND);
    }


    public ResponseEntity<?> clearCart(Integer userId) {
        ticketRepository.clearCartOfUser(userId);
        return ResponseEntity.noContent().build();
    }


    public ResponseEntity<?> deleteTicketFromCart(Integer userId, Integer ticketId) {
        try {
            ticketRepository.deleteTicketFromCart(userId, ticketId);
        } catch (Exception ignored) {
        }
        return ResponseEntity.ok("Successfully deleted from the cart!");
    }
}
