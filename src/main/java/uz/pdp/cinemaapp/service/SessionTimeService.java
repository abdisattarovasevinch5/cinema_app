package uz.pdp.cinemaapp.service;
//Sevinch Abdisattorova 03/17/2022 4:39 PM


import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.cinemaapp.entity.SessionTime;
import uz.pdp.cinemaapp.repository.SessionTimeRepository;

import java.sql.Time;
import java.util.List;
import java.util.Optional;


@RequiredArgsConstructor
@Service
public class SessionTimeService {

    private final SessionTimeRepository sessionTimeRepository;

    public List<SessionTime> getAllTimes() {
        return sessionTimeRepository.findAll();
    }

    public SessionTime getTimeById(Integer id) {
        Optional<SessionTime> byId = sessionTimeRepository.findById(id);
        return byId.orElse(null);
    }

    public ResponseEntity<?> addTime(String time) {
        Time time1 = Time.valueOf(time);
        SessionTime byTime = sessionTimeRepository.findDistinctFirstByTime(time1);
        if (byTime != null) {
            return ResponseEntity.ok(" time id: " + byTime.getId());
        }
        return ResponseEntity.ok(" time id: " + sessionTimeRepository.save(new SessionTime(time1)).getId());
    }

    public boolean deleteTimeById(Integer id) {
        try {
            sessionTimeRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean editTime(String time, Integer id) {
        Optional<SessionTime> byId = sessionTimeRepository.findById(id);
        if (byId.isPresent()) {
            SessionTime sessionTime = byId.get();
            sessionTime.setTime(Time.valueOf(time));
            sessionTimeRepository.save(sessionTime);
            return true;
        }
        return false;
    }
}
