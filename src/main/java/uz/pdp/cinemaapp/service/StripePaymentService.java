package uz.pdp.cinemaapp.service;
//Sevinch Abdisattorova 03/28/2022 2:23 PM

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.checkout.Session;
import com.stripe.param.checkout.SessionCreateParams;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.cinemaapp.dto.StripeResponseDto;
import uz.pdp.cinemaapp.projection.CustomTicket;

import java.util.ArrayList;
import java.util.List;

@Service
public class StripePaymentService {

    @Value("${STRIPE_SECRET_KEY}")
    String stripeApiKey;

    @Value("${BASE_URL}")
    String baseUrl;


    public ResponseEntity<?> createStripeSession(List<CustomTicket> tickets, Integer userId) throws StripeException {
        // SUCCESS and FAILURE URLS
        String successURL = baseUrl + "success";
        String failureURL = baseUrl + "failed";

        Stripe.apiKey = stripeApiKey;
        List<SessionCreateParams.LineItem> sessionItemList = new ArrayList<>();

        for (CustomTicket ticket : tickets) {
            sessionItemList.add(createSessionLineItem(ticket));
        }
        SessionCreateParams params = SessionCreateParams.builder()
                .addPaymentMethodType(SessionCreateParams.PaymentMethodType.CARD)
                .setMode(SessionCreateParams.Mode.PAYMENT)
                .setCancelUrl(failureURL)
                .setSuccessUrl(successURL)
                .addAllLineItem(sessionItemList)
                .setClientReferenceId(userId.toString())
                .build();

        Session session = Session.create(params);

        // returning session url
        StripeResponseDto stripeResponse = new StripeResponseDto(session.getUrl());
        return ResponseEntity.ok(stripeResponse);

    }

    private SessionCreateParams.LineItem createSessionLineItem(CustomTicket ticket) {
        return SessionCreateParams
                .LineItem
                .builder()
                .setPriceData(createPriceData(ticket))
                .setQuantity(1L)
                .build();
    }

    private SessionCreateParams.LineItem.PriceData createPriceData(CustomTicket ticket) {
        double chargeAmount = (ticket.getPrice() * 100 + 30) / (1 - 2.9 / 100);
        return SessionCreateParams.LineItem.PriceData.builder()
                .setCurrency("usd")
                .setUnitAmount((long) (chargeAmount))
                .setProductData(SessionCreateParams.LineItem.PriceData.ProductData.builder()
                        .setName(ticket.getMovieName())
                        .setDescription(
                                "hall: " + ticket.getHallName() +
                                        ", row: " + ticket.getRowNumber() +
                                        ", seat: " + ticket.getSeatNumber())
                        .build())
                .build();
    }


}
