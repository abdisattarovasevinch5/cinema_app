package uz.pdp.cinemaapp.service;
//Sevinch Abdisattorova 03/27/2022 7:25 PM

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.cinemaapp.entity.NightSessionAddFee;
import uz.pdp.cinemaapp.repository.NightSessionFeeRepository;

import java.sql.Time;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class NightSessionFeeService {

    private final NightSessionFeeRepository nightSessionFeeRepository;


    public ResponseEntity<?> getNightSessionFee() {
        List<NightSessionAddFee> all = nightSessionFeeRepository.findAll();
        return ResponseEntity.ok(all);
    }


    public ResponseEntity<?> updateNightSessionFee(Integer percent, String time, Integer id) {
        Optional<NightSessionAddFee> byId = nightSessionFeeRepository.findById(id);
        if (byId.isPresent()) {
            NightSessionAddFee nightSessionAddFee1 = byId.get();
            if (percent != null) {
                nightSessionAddFee1.setPercentage(percent);
            }
            if (time != null) {
                nightSessionAddFee1.setTime(Time.valueOf(time));
            }
            nightSessionFeeRepository.save(nightSessionAddFee1);
            return ResponseEntity.ok("Successfully updated!");
        }
        return new ResponseEntity("Not found", HttpStatus.NOT_FOUND);

    }


    public ResponseEntity<?> addNightSessionFee(Integer percent, String time) {
        NightSessionAddFee nightSessionAddFee = new NightSessionAddFee(percent, Time.valueOf(time));
        NightSessionAddFee save = nightSessionFeeRepository.save(nightSessionAddFee);
        return new ResponseEntity("Successfully created!", HttpStatus.CREATED);
    }


    public ResponseEntity<?> deleteNightSessionFee(Integer id) {
        try {
            nightSessionFeeRepository.deleteById(id);
            return ResponseEntity.noContent().build();
        } catch (Exception ignored) {
        }
        return ResponseEntity.notFound().build();

    }
}
