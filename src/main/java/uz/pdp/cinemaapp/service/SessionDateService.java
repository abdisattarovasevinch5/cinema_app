package uz.pdp.cinemaapp.service;
//Sevinch Abdisattorova 03/17/2022 4:39 PM


import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.cinemaapp.entity.SessionDate;
import uz.pdp.cinemaapp.repository.SessionDateRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class SessionDateService {

    private final SessionDateRepository sessionDateRepository;

    public List<SessionDate> getAllDates() {
        return sessionDateRepository.findAll();
    }

    public SessionDate getDateById(Integer id) {
        Optional<SessionDate> byId = sessionDateRepository.findById(id);
        return byId.orElse(null);
    }

    public ResponseEntity<?> addDate(String date) {
        LocalDate date1 = LocalDate.parse(date);
        SessionDate byDate = sessionDateRepository.findDistinctFirstByDate(date1);
        if (byDate != null) {
            return ResponseEntity.ok(" date id: " + byDate.getId());
        }
        return ResponseEntity.ok(" date id: " + sessionDateRepository.save(new SessionDate(date1)).getId());
    }

    public boolean deleteDateById(Integer id) {
        try {
            sessionDateRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean editDate(String date, Integer id) {
        Optional<SessionDate> byId = sessionDateRepository.findById(id);
        if (byId.isPresent()) {
            SessionDate sessionDate = byId.get();
            sessionDate.setDate(LocalDate.parse(date));
            sessionDateRepository.save(sessionDate);
            return true;
        }
        return false;
    }
}
