package uz.pdp.cinemaapp.service;
//Sevinch Abdisattorova 03/16/2022 12:03 AM

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.pdp.cinemaapp.entity.Genre;
import uz.pdp.cinemaapp.repository.GenreRepository;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class GenreService {


    private final GenreRepository genreRepository;

    public List<Genre> getGenresByIds(List<Integer> ids) {
        return genreRepository.findAllById(ids);
    }


    public void deleteGenresOfMovie(Integer id) {
        genreRepository.deleteGenresOfMovie(id);
    }


    public List<Genre> getAllGenres() {
        List<Genre> all = genreRepository.findAll();
        return all;
    }


    public Genre getGenreById(Integer id) {
        Optional<Genre> byId = genreRepository.findById(id);
        return byId.orElse(null);
    }


    public boolean addGenre(Genre genre) {
        Genre savedGenre = genreRepository.save(genre);
        return savedGenre != null;
    }


    public boolean editGenre(Genre genre, Integer id) {
        Optional<Genre> byId = genreRepository.findById(id);
        if (byId.isPresent()) {
            Genre genre1 = byId.get();
            if (genre.getName() != null) {
                genre1.setName(genre.getName());
            }
            genreRepository.save(genre1);
            return true;
        }
        return false;
    }


    public boolean deleteGenreById(Integer id) {
        try {
            genreRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
