package uz.pdp.cinemaapp.dto;
//Sevinch Abdisattorova 03/28/2022 11:20 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MovieAnnouncementDto {
    Integer movieId;
    Boolean isActive;
}
