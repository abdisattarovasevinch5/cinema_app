package uz.pdp.cinemaapp.dto;
//Sevinch Abdisattorova 03/28/2022 2:32 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class StripeResponseDto {
    private String sessionId;

}

