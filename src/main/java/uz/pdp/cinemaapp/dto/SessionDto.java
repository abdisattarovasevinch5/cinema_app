package uz.pdp.cinemaapp.dto;
//Sevinch Abdisattorova 03/26/2022 9:02 AM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
public class SessionDto {

    Integer movieAnnouncementId;
    List<ReservedHallDto> reservedHallDtoList;
//    String endTime;


}
