package uz.pdp.cinemaapp.dto;
//Sevinch Abdisattorova 03/26/2022 9:25 AM


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReservedHallDto {
    Integer hallId;
    String startDate;
    String endDate;//null
    List<String> startTimeList;
}
