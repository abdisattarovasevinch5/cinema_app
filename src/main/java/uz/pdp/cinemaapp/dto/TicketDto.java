package uz.pdp.cinemaapp.dto;
//Sevinch Abdisattorova 03/28/2022 2:25 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
public class TicketDto {
    Integer movieSessionId;
    Integer seatId;
    double price;
    String movieTitle;
}
