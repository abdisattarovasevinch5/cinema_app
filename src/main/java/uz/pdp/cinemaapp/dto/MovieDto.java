package uz.pdp.cinemaapp.dto;
//Sevinch Abdisattorova 03/15/2022 11:18 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

// TODO: 04/05/2022 add validation
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MovieDto {
    String title;

    String description;

    Integer durationInMinutes;

    MultipartFile coverImage;

    MultipartFile trailerVideo;

    List<Integer> directorIds;

    List<Integer> actorIds;

    List<MultipartFile> photos;

    List<Integer> genres;

    Integer distributorId;

    Integer distributorShareInPercent;

    Double minPrice;

    String releaseDate;

    Double budget;

}
