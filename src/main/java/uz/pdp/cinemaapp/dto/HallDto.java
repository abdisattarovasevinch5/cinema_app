package uz.pdp.cinemaapp.dto;
//Sevinch Abdisattorova 03/17/2022 6:32 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.cinemaapp.projection.CustomRowSeats;

import javax.validation.Valid;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
public class HallDto {
    String name;
    Integer vipAdditionalFeeInPercent;
    List<CustomRowSeats> rows;

}
