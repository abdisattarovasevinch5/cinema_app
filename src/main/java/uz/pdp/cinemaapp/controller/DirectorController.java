package uz.pdp.cinemaapp.controller;
//Sevinch Abdisattorova 03/14/2022 11:02 PM

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinemaapp.entity.Director;
import uz.pdp.cinemaapp.service.DirectorService;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/director")
public class DirectorController {

    private final DirectorService directorService;


    @GetMapping
    public ResponseEntity<?> getAllDistributors() {
        List<Director> directors = directorService.getAllDirectors();
        return ResponseEntity.ok(directors);
    }


    @GetMapping("/{id}")
    public ResponseEntity<?> getDistributorById(@PathVariable Integer id) {
        Director director = directorService.getDirectorById(id);
        return director != null ? ResponseEntity.ok(director) :
                ResponseEntity.notFound().build();
    }


    @PostMapping()
    public ResponseEntity<?> addDistributor(@RequestBody Director director) {
        boolean result = directorService.addDirector(director);
        return result ?
                new ResponseEntity("Successfully saved", HttpStatus.CREATED)
                :
                new ResponseEntity("Could not save", HttpStatus.CONFLICT);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> editDistributor(@PathVariable Integer id, @RequestBody Director director) {
        boolean result = directorService.editDirector(director, id);
        return result ?
                new ResponseEntity("Successfully updated", HttpStatus.CREATED)
                :
                new ResponseEntity("Could not update", HttpStatus.CONFLICT);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteDistributor(@PathVariable Integer id) {
        boolean result = directorService.deleteDirectorById(id);
        if (result)
            return ResponseEntity.noContent().build();
        return
                ResponseEntity.notFound().build();
    }
}
