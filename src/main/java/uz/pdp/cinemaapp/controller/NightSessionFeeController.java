package uz.pdp.cinemaapp.controller;
//Sevinch Abdisattorova 03/14/2022 11:02 PM

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinemaapp.service.NightSessionFeeService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/night-session-fee")
public class NightSessionFeeController {


    private final NightSessionFeeService nightSessionFeeService;


    @GetMapping
    public ResponseEntity<?> getNightSessionFee() {
        return nightSessionFeeService.getNightSessionFee();
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> updateNightSessionFee(@PathVariable Integer id, Integer percent, String time) {
        return nightSessionFeeService.updateNightSessionFee(percent, time, id);
    }


    @PostMapping()
    public ResponseEntity<?> addNightSessionFee(Integer percent, String time) {
        return nightSessionFeeService.addNightSessionFee(percent, time);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteNightSessionFee(@PathVariable Integer id) {
        return nightSessionFeeService.deleteNightSessionFee(id);
    }


}
