package uz.pdp.cinemaapp.controller;
//Sevinch Abdisattorova 03/14/2022 11:02 PM

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.cinemaapp.service.TransactionHistoryService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/transaction-history")
public class TransactionHistoryController {


    private final TransactionHistoryService transactionHistoryService;


    @GetMapping
    public ResponseEntity<?> getAllTransactionHistories() {
        return transactionHistoryService.getAllTransactionHistories();
    }


    @GetMapping("/{userId}")
    public ResponseEntity<?> getAllTransactionHistoriesOfUser(
            @PathVariable Integer userId) {
        return transactionHistoryService.getAllTransactionHistoriesOfUser(userId);
    }


}
