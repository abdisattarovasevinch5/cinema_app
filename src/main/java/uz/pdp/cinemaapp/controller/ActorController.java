package uz.pdp.cinemaapp.controller;
//Sevinch Abdisattorova 03/14/2022 11:02 PM

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinemaapp.entity.Actor;
import uz.pdp.cinemaapp.service.ActorService;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/actors")
public class ActorController {

    private final ActorService actorService;


    @GetMapping
    public ResponseEntity<?> getActors() {
        List<Actor> allActors = actorService.getAllActors();
        return ResponseEntity.ok(allActors);
    }


    @GetMapping("/{id}")
    public ResponseEntity<?> getActorById(@PathVariable Integer id) {
        Actor actorById = actorService.getActorById(id);
        return actorById != null ? ResponseEntity.ok(actorById) :
                ResponseEntity.notFound().build();
    }


    @PostMapping(consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> saveActor(
            Actor actor
    ) {
        boolean result = actorService.saveActor(actor);
        return result ?
                new ResponseEntity("Successfully saved", HttpStatus.CREATED)
                :
                new ResponseEntity("Could not save", HttpStatus.CONFLICT);
    }


    @PutMapping(value = "/{id}", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> editActor(@PathVariable Integer id, Actor actor) {
        Actor actor1 = actorService.editActor(id, actor);
        if (actor1 != null) {
            return ResponseEntity.ok("Successfully updated!");
        }
        return ResponseEntity.notFound().build();
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteActor(@PathVariable Integer id) {
        boolean result = actorService.deleteActor(id);
        if (result)
            return ResponseEntity.noContent().build();
        return
                ResponseEntity.notFound().build();

    }
}
