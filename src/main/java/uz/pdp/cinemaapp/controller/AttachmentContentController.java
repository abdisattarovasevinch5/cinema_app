package uz.pdp.cinemaapp.controller;
//Sevinch Abdisattorova 03/14/2022 11:02 PM

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinemaapp.entity.AttachmentContent;
import uz.pdp.cinemaapp.service.AttachmentContentService;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api/attachment-contents")
public class AttachmentContentController {

    @Autowired
    AttachmentContentService attachmentContentService;

    @CrossOrigin
    @GetMapping("/{id}")
    public ResponseEntity<?> getAttachmentContentByAttachmentId(@PathVariable Integer id) {
        AttachmentContent attachmentContent = attachmentContentService.getAttachmentContentByAttachmentId(id);
        return attachmentContent != null ? ResponseEntity.ok(attachmentContent) :
                ResponseEntity.notFound().build();
    }

    @CrossOrigin
    @GetMapping("/data/{id}")
    public ResponseEntity<?> getAttachmentContentId(@PathVariable Integer id) {
        byte[] attachmentContentData = attachmentContentService.getAttachmentContentId(id);
        return attachmentContentData != null ? ResponseEntity.ok(attachmentContentData) :
                ResponseEntity.notFound().build();
    }

    @GetMapping("/file/{id}")
    public void getFile(@PathVariable Integer id, HttpServletResponse response) {
        attachmentContentService.getFile(id, response);

    }
}
