package uz.pdp.cinemaapp.controller;
//Sevinch Abdisattorova 03/14/2022 11:02 PM

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinemaapp.entity.SessionTime;
import uz.pdp.cinemaapp.service.SessionTimeService;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/time")
public class TimeController {

    private final SessionTimeService sessionTimeService;


    @GetMapping
    public ResponseEntity<?> getAllTimes() {
        List<SessionTime> times = sessionTimeService.getAllTimes();
        return ResponseEntity.ok(times);
    }


    @GetMapping("/{id}")
    public ResponseEntity<?> getTimeById(@PathVariable Integer id) {
        SessionTime timeById = sessionTimeService.getTimeById(id);
        return timeById != null ? ResponseEntity.ok(timeById) :
                ResponseEntity.notFound().build();
    }


    @PostMapping()
    public ResponseEntity<?> addTime(@RequestParam String time) {
        return sessionTimeService.addTime(time);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> editTime(@PathVariable Integer id, @RequestParam String time) {
        boolean result = sessionTimeService.editTime(time, id);
        return result ?
                new ResponseEntity("Successfully updated", HttpStatus.CREATED)
                :
                new ResponseEntity("Could not update", HttpStatus.CONFLICT);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteTimeById(@PathVariable Integer id) {
        boolean result = sessionTimeService.deleteTimeById(id);
        if (result)
            return ResponseEntity.noContent().build();
        return
                ResponseEntity.notFound().build();
    }
}
