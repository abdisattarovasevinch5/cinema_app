package uz.pdp.cinemaapp.controller;
//Sevinch Abdisattorova 03/14/2022 11:02 PM

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinemaapp.dto.HallDto;
import uz.pdp.cinemaapp.projection.CustomHallById;
import uz.pdp.cinemaapp.projection.CustomHallWithVIPFee;
import uz.pdp.cinemaapp.repository.HallRepository;
import uz.pdp.cinemaapp.service.HallService;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/hall")
public class HallController {


    private final HallService hallService;


    @GetMapping
    public ResponseEntity<?> getAllHalls() {
        List<CustomHallWithVIPFee> allHalls = hallService.getAllHalls();
        return ResponseEntity.ok(allHalls);
    }


    @GetMapping("/{id}")
    public ResponseEntity<?> getHallById(@PathVariable Integer id) {
        CustomHallById hallById = hallService.getHallById(id);
        return hallById != null ? ResponseEntity.ok(hallById) :
                ResponseEntity.notFound().build();
    }


    @PostMapping()
    public ResponseEntity<?> addHall(@RequestBody HallDto hall) {
        return hallService.addHall(hall);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> editHall(@PathVariable Integer id, @RequestBody HallDto hall) {
        boolean result = hallService.editHall(hall, id);
        return result ?
                new ResponseEntity("Successfully updated", HttpStatus.CREATED)
                :
                new ResponseEntity("Could not update", HttpStatus.CONFLICT);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteHallById(@PathVariable Integer id) {
        boolean result = hallService.deleteHallById(id);
        if (result)
            return ResponseEntity.noContent().build();
        return
                ResponseEntity.notFound().build();
    }
}
