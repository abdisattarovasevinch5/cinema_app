package uz.pdp.cinemaapp.controller;
//Sevinch Abdisattorova 03/17/2022 10:30 AM

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinemaapp.dto.MovieAnnouncementDto;
import uz.pdp.cinemaapp.service.MovieAnnouncementService;


@RequiredArgsConstructor
@RestController()
@RequestMapping(path = "api/movie-announcement")
public class MovieAnnouncementController {


    private final MovieAnnouncementService movieAnnouncementService;


   /* @GetMapping
    public ResponseEntity getAllMovieAnnouncement(
            @RequestParam(name = "page", defaultValue = "1", required = false) Integer page,
            @RequestParam(name = "size", defaultValue = "10", required = false) Integer size,
            @RequestParam(name = "search", defaultValue = "", required = false) String search,
            @RequestParam(name = "sort", defaultValue = "startDate", required = false) String sort,
            @RequestParam(name = "direction", defaultValue = "true", required = false) boolean direction
    ) {
        return movieAnnouncementService.getAllMovieAnnouncements(page, size, search, sort, direction);
    }*/

    @GetMapping
    public ResponseEntity<?> getAllMovieAnnouncements() {
        return movieAnnouncementService.getAllMovieAnnouncements();
    }

    @GetMapping("/{movieAnnouncementId}")
    public ResponseEntity<?> getMovieAnnouncementById(@PathVariable Integer movieAnnouncementId) {
        return movieAnnouncementService.getMovieAnnouncementById(movieAnnouncementId);
    }

    @PostMapping
    public ResponseEntity<?> makeMovieAnnouncement(@RequestBody MovieAnnouncementDto movieAnnouncementDto) {
        return movieAnnouncementService.makeMovieAnnouncement(movieAnnouncementDto);
    }


    @PutMapping("/{afishaId}")
    public ResponseEntity<?> editMovieAnnouncement(MovieAnnouncementDto movieAnnouncementDto, @PathVariable Integer afishaId) {
        return movieAnnouncementService.editMovieAnnouncement(movieAnnouncementDto, afishaId);
    }


    @DeleteMapping("/{afishaId}")
    public ResponseEntity<?> deleteMovieAnnouncementById(@PathVariable Integer afishaId) {
        return movieAnnouncementService.deleteMovieAnnouncement(afishaId);
    }
}
