package uz.pdp.cinemaapp.controller;
//Sevinch Abdisattorova 03/14/2022 11:02 PM

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinemaapp.entity.PayType;
import uz.pdp.cinemaapp.service.PayTypeService;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/pay-type")
public class PayTypeController {

    private final PayTypeService payTypeService;


    @GetMapping
    public ResponseEntity<?> getAllPayTypes() {
        List<PayType> payTypes = payTypeService.getAllPayTypes();
        return ResponseEntity.ok(payTypes);
    }


    @GetMapping("/{id}")
    public ResponseEntity<?> getPayTypeById(@PathVariable Integer id) {
        PayType payTypeById = payTypeService.getPayTypeById(id);
        return payTypeById != null ? ResponseEntity.ok(payTypeById) :
                ResponseEntity.notFound().build();
    }


    @PostMapping()
    public ResponseEntity<?> addPayType(PayType payType) {
        return payTypeService.addPayType(payType);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> editPayType(@PathVariable Integer id, PayType payType) {
        boolean result = payTypeService.editPayType(payType, id);
        return result ?
                new ResponseEntity("Successfully updated", HttpStatus.CREATED)
                :
                new ResponseEntity("Could not update", HttpStatus.CONFLICT);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletePayTypeById(@PathVariable Integer id) {
        boolean result = payTypeService.deletePayTypeById(id);
        if (result)
            return ResponseEntity.noContent().build();
        return
                ResponseEntity.notFound().build();
    }
}
