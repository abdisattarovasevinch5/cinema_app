package uz.pdp.cinemaapp.controller;
//Sevinch Abdisattorova 03/14/2022 11:02 PM

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.cinemaapp.service.AvailableSeatsService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/available-seats")
public class AvailableSeatsController {

    private final AvailableSeatsService availableSeatsService;


    @GetMapping
    @RequestMapping("/{movieSessionId}")
    public ResponseEntity<?> getAvailableSeats(@PathVariable Integer movieSessionId) {
        return availableSeatsService.getAllAvailableSeats(movieSessionId);
    }



}
