package uz.pdp.cinemaapp.controller;
//Sevinch Abdisattorova 03/14/2022 11:02 PM

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinemaapp.dto.MovieDto;
import uz.pdp.cinemaapp.projection.CustomMovie;
import uz.pdp.cinemaapp.projection.CustomMovieById;
import uz.pdp.cinemaapp.service.MovieService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/movies")
public class MovieController {

    private final MovieService movieService;


    @CrossOrigin
    @GetMapping
    public ResponseEntity<?> getMovies(
            @RequestParam(name = "page", defaultValue = "1", required = false) Integer page,
            @RequestParam(name = "size", defaultValue = "10", required = false) Integer size,
            @RequestParam(name = "search", defaultValue = "", required = false) String search
    ) {

        Page<CustomMovie> allMovies = movieService.getAllMovies(page, size, search);
        return ResponseEntity.ok(allMovies);
    }


    @CrossOrigin
    @GetMapping("/{id}")
    public ResponseEntity<?> getMovieById(@PathVariable Integer id) {
        CustomMovieById movieById = movieService.getMovieById(id);
        return movieById != null ? ResponseEntity.ok(movieById) :
                ResponseEntity.notFound().build();
    }


    @PostMapping(consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> saveMovie(
            MovieDto movie
    ) {
        boolean result = movieService.saveMovie(movie);
        // TODO: 04/05/2022 check

        if (result)
            return new ResponseEntity("Successfully  saved", HttpStatus.CREATED);
        return new ResponseEntity("Could not save", HttpStatus.CONFLICT);
    }

    @PutMapping(path = "/{id}", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> editMovie(
            @PathVariable Integer id,
            MovieDto movie
    ) {
        return movieService.editMovie(id, movie);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteMovie(@PathVariable Integer id) {
        boolean result = movieService.deleteMovie(id);
        if (result)
            return ResponseEntity.noContent().build();
        return
                ResponseEntity.notFound().build();
    }
}
