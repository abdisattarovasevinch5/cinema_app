package uz.pdp.cinemaapp.controller;
//Sevinch Abdisattorova 03/14/2022 11:02 PM

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinemaapp.service.PaymentService;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/ticket")
public class PaymentController {


    private final PaymentService paymentService;

    @CrossOrigin
    @GetMapping("/purchase/{userId}")
    public ResponseEntity<?> transactionTicket(@PathVariable Integer userId) {
        return paymentService.purchaseTicket(userId, 1);
    }

    @PostMapping("/refund")
    public ResponseEntity<?> refundTickets(@RequestBody List<Integer> ticketIds) {
        return paymentService.refundTickets(ticketIds);
    }


}
