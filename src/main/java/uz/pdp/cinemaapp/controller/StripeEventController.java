package uz.pdp.cinemaapp.controller;
//Sevinch Abdisattorova 03/31/2022 9:27 AM

import com.stripe.Stripe;
import com.stripe.model.Event;
import com.stripe.model.StripeObject;
import com.stripe.model.checkout.Session;
import com.stripe.net.Webhook;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinemaapp.entity.Ticket;
import uz.pdp.cinemaapp.entity.enums.TicketStatus;
import uz.pdp.cinemaapp.repository.TicketRepository;
import uz.pdp.cinemaapp.service.MailService;
import uz.pdp.cinemaapp.service.TicketService;
import uz.pdp.cinemaapp.service.TransactionHistoryService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/stripe-webhook")
@RequiredArgsConstructor
public class StripeEventController {


    @Value("${STRIPE_SECRET_KEY}")
    String stripeApiKey;

    @Value("${WEBHOOK_SECRET_KEY}")
    String webhookSecretKey;


    private final MailService mailService;
    private final TransactionHistoryService transactionHistoryService;
    private final TicketService ticketService;
    private final TicketRepository ticketRepository;


    @PostMapping
    public void handle(@RequestBody String payload,
                       @RequestHeader(name = "Stripe-Signature") String sigHeader) {
        Stripe.apiKey = stripeApiKey;
        String endpointSecret = webhookSecretKey;
        Event event = null;

        try {
            event = Webhook.constructEvent(payload, sigHeader, endpointSecret);
            System.out.println("Event type: " + event.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }


        assert event != null;
        if ("checkout.session.completed".equals(event.getType())) {
            Optional<StripeObject> object = event.getDataObjectDeserializer().getObject();
            if (object.isPresent()) {
                Session session = (Session) object.get();
                fulfillOrder(session);
            }

            System.out.println("Got payload: " + payload);
        }

    }


    public void fulfillOrder(Session session) {

        String clientReferenceId = session.getClientReferenceId();
        Integer userId = Integer.valueOf(clientReferenceId);

        String paymentIntent = session.getPaymentIntent();
        double amountTotal = session.getAmountTotal().doubleValue() / 100;
        List<Ticket> tickets = ticketRepository.findByUserIdAndStatus(userId, TicketStatus.NEW);
        transactionHistoryService.saveTransactionHistory(tickets, 1, amountTotal, paymentIntent, false);
        mailService.sendEmailToClient(session, userId);
        ticketService.changeTicketToPurchasedAndGenerateQrCode(userId);

    }
}
