package uz.pdp.cinemaapp.controller;
//Sevinch Abdisattorova 03/14/2022 11:02 PM

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinemaapp.service.CartService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/cart")
public class CartController {


    private final CartService cartService;


    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/{userId}")
    public ResponseEntity<?> showTicketsInTheCart(@PathVariable Integer userId) {
        return cartService.getTicketsInTheCart(userId);
    }


    @PostMapping
    @RequestMapping("/{userId}")
    public ResponseEntity<?> addTicketToCart(@PathVariable Integer userId, Integer seatId, Integer movieSessionId) {

        return cartService.addToCart(userId, seatId, movieSessionId);
    }


    @DeleteMapping("/{userId}")
    public ResponseEntity<?> clearCart(@PathVariable Integer userId) {
        return cartService.clearCart(userId);
    }


    @DeleteMapping("/ticket/{userId}/{ticketId}")
    public ResponseEntity<?> deleteTicketFromCart(@PathVariable Integer userId, @PathVariable Integer ticketId) {
        return cartService.deleteTicketFromCart(userId, ticketId);
    }
}
