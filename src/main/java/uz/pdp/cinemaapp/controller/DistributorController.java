package uz.pdp.cinemaapp.controller;
//Sevinch Abdisattorova 03/14/2022 11:02 PM

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinemaapp.entity.Distributor;
import uz.pdp.cinemaapp.service.DistributorService;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/distributors")
public class DistributorController {

    private final DistributorService distributorService;


    @GetMapping
    public ResponseEntity<?> getAllDistributors() {
        List<Distributor> distributors = distributorService.getAllDistributors();
        return ResponseEntity.ok(distributors);
    }


    @GetMapping("/{id}")
    public ResponseEntity<?> getDistributorById(@PathVariable Integer id) {
        Distributor distributorById = distributorService.getDistributorById(id);
        return distributorById != null ? ResponseEntity.ok(distributorById) :
                ResponseEntity.notFound().build();
    }


    @PostMapping()
    public ResponseEntity<?> addDistributor(@RequestBody Distributor distributor) {
        boolean result = distributorService.addDistributor(distributor);
        return result ?
                new ResponseEntity("Successfully saved", HttpStatus.CREATED)
                :
                new ResponseEntity("Could not save", HttpStatus.CONFLICT);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> editDistributor(@PathVariable Integer id, @RequestBody Distributor distributor) {
        boolean result = distributorService.editDistributor(distributor, id);
        return result ?
                new ResponseEntity("Successfully updated", HttpStatus.CREATED)
                :
                new ResponseEntity("Could not update", HttpStatus.CONFLICT);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteDistributor(@PathVariable Integer id) {
        boolean result = distributorService.deleteDistributorById(id);
        if (result)
            return ResponseEntity.noContent().build();
        return
                ResponseEntity.notFound().build();
    }
}
