package uz.pdp.cinemaapp.controller;
//Sevinch Abdisattorova 03/14/2022 11:02 PM

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinemaapp.entity.SessionDate;
import uz.pdp.cinemaapp.service.SessionDateService;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/date")
public class DateController {

    private final SessionDateService sessionDateService;


    @GetMapping
    public ResponseEntity<?> getAllDates() {
        List<SessionDate> dates = sessionDateService.getAllDates();
        return ResponseEntity.ok(dates);
    }


    @GetMapping("/{id}")
    public ResponseEntity<?> getDateById(@PathVariable Integer id) {
        SessionDate dateById = sessionDateService.getDateById(id);
        return dateById != null ? ResponseEntity.ok(dateById) :
                ResponseEntity.notFound().build();
    }


    @PostMapping()
    public ResponseEntity<?> addDate(@RequestParam String date) {
        return sessionDateService.addDate(date);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> editDate(@PathVariable Integer id, @RequestParam String date) {
        boolean result = sessionDateService.editDate(date, id);
        return result ?
                new ResponseEntity("Successfully updated", HttpStatus.CREATED)
                :
                new ResponseEntity("Could not update", HttpStatus.CONFLICT);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteDateById(@PathVariable Integer id) {
        boolean result = sessionDateService.deleteDateById(id);
        if (result)
            return ResponseEntity.noContent().build();
        return
                ResponseEntity.notFound().build();
    }
}
