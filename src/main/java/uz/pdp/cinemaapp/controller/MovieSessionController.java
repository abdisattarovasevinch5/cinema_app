package uz.pdp.cinemaapp.controller;
//Sevinch Abdisattorova 03/18/2022 10:54 AM


import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.cinemaapp.service.MovieSessionService;

@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/api/movie-session")
public class MovieSessionController {

    private final MovieSessionService movieSessionService;

    @GetMapping
    public ResponseEntity<?> getAllMovieSessions(
            @RequestParam(name = "page", defaultValue = "1", required = false) Integer page,
            @RequestParam(name = "size", defaultValue = "10", required = false) Integer size,
            @RequestParam(name = "search", defaultValue = "", required = false) String search
    ) {
        return movieSessionService.getAllMovieSessions(page, size, search);
    }
}
