package uz.pdp.cinemaapp.controller;
//Sevinch Abdisattorova 03/14/2022 11:02 PM

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinemaapp.entity.PriceCategory;
import uz.pdp.cinemaapp.service.PriceCategoryService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/price-category")
public class PriceCategoryController {

    private final PriceCategoryService priceCategoryService;


    @GetMapping
    public ResponseEntity<?> getAllPriceCategories() {
        return priceCategoryService.getAllPriceCategories();
    }


    @GetMapping("/{id}")
    public ResponseEntity<?> getPriceCategoryById(@PathVariable Integer id) {
        return priceCategoryService.getPriceCategoryById(id);
    }


    @PostMapping()
    public ResponseEntity<?> addPriceCategory(@RequestBody PriceCategory priceCategory) {
        return priceCategoryService.addPriceCategory(priceCategory);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> editPriceCategory(@PathVariable Integer id, @RequestBody PriceCategory priceCategory) {
        return priceCategoryService.editPriceCategory(id, priceCategory);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletePriceCategoryById(@PathVariable Integer id) {
        return priceCategoryService.deletePriceCategoryById(id);

    }
}
