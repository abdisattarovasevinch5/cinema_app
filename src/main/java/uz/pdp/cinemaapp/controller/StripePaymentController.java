package uz.pdp.cinemaapp.controller;
//Sevinch Abdisattorova 03/31/2022 8:50 AM


import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class StripePaymentController {

    @RequestMapping("/success")
    public ResponseEntity<?> getSuccessMsg() {
        return ResponseEntity.ok("Operation successfully completed!");
    }


}
