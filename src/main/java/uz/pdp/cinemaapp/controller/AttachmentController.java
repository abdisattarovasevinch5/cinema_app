package uz.pdp.cinemaapp.controller;
//Sevinch Abdisattorova 03/14/2022 11:02 PM

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.cinemaapp.entity.Attachment;
import uz.pdp.cinemaapp.service.AttachmentService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/attachments")
public class AttachmentController {

    private final AttachmentService attachmentService;


    @GetMapping("/{id}")
    public ResponseEntity<?> getAttachmentById(@PathVariable Integer id) {
        Attachment attachmentById = attachmentService.getAttachmentById(id);
        return attachmentById != null ? ResponseEntity.ok(attachmentById) :
                ResponseEntity.notFound().build();
    }


}
