package uz.pdp.cinemaapp.controller;
//Sevinch Abdisattorova 03/14/2022 11:02 PM

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinemaapp.entity.Genre;
import uz.pdp.cinemaapp.service.GenreService;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/genres")
public class GenreController {

    private final GenreService genreService;


    @GetMapping
    public ResponseEntity<?> getAllGenres() {
        List<Genre> genres = genreService.getAllGenres();
        return ResponseEntity.ok(genres);
    }


    @GetMapping("/{id}")
    public ResponseEntity<?> getGenreById(@PathVariable Integer id) {
        Genre genreById = genreService.getGenreById(id);
        return genreById != null ? ResponseEntity.ok(genreById) :
                ResponseEntity.notFound().build();
    }


    @PostMapping()
    public ResponseEntity<?> addGenre(@RequestBody Genre genre) {
        boolean result = genreService.addGenre(genre);
        return result ?
                new ResponseEntity("Successfully saved", HttpStatus.CREATED)
                :
                new ResponseEntity("Could not save", HttpStatus.CONFLICT);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> editGenre(@PathVariable Integer id, @RequestBody Genre genre) {
        boolean result = genreService.editGenre(genre, id);
        return result ?
                new ResponseEntity("Successfully updated", HttpStatus.CREATED)
                :
                new ResponseEntity("Could not update", HttpStatus.CONFLICT);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteGenreById(@PathVariable Integer id) {
        boolean result = genreService.deleteGenreById(id);
        if (result)
            return ResponseEntity.noContent().build();
        return
                ResponseEntity.notFound().build();
    }
}
