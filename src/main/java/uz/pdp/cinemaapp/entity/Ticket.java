package uz.pdp.cinemaapp.entity;
//Sevinch Abdisattorova 03/14/2022 6:45 PM


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.cinemaapp.entity.enums.TicketStatus;
import uz.pdp.cinemaapp.entity.template.AbsEntity;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "tickets")
@PackagePrivate
public class Ticket extends AbsEntity {

    @ManyToOne
    MovieSession movieSession;

    @ManyToOne
    Seat seat;

    @OneToOne
    Attachment qrCode;

    Double price;

    @Enumerated(EnumType.STRING)
    TicketStatus status;

    @ManyToOne
    User user;


}
