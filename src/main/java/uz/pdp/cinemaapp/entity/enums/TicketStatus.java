package uz.pdp.cinemaapp.entity.enums;

public enum TicketStatus {
    NEW,
    PURCHASED,
    REFUNDED;
}
