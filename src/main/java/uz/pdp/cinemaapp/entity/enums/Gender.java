package uz.pdp.cinemaapp.entity.enums;

public enum Gender {
    MALE,
    FEMALE;
}
