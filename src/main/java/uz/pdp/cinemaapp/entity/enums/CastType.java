package uz.pdp.cinemaapp.entity.enums;

public enum CastType {
    ACTOR,
    DIRECTOR;
}
