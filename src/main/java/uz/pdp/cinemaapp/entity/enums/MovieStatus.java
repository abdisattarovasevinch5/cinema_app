package uz.pdp.cinemaapp.entity.enums;

public enum MovieStatus {
    ACTIVE,
    DISABLED,
    PASSED,
    SOON;
}
