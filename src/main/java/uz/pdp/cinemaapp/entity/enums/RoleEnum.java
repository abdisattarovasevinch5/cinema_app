package uz.pdp.cinemaapp.entity.enums;

public enum RoleEnum {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_DIRECTOR
}
