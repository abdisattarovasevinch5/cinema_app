package uz.pdp.cinemaapp.entity;
//Sevinch Abdisattorova 03/14/2022 6:39 PM


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.cinemaapp.entity.template.AbsEntity;

import javax.persistence.Entity;
import java.sql.Time;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "night_session_add_fees")
@PackagePrivate
public class NightSessionAddFee extends AbsEntity {

    Integer percentage;

    Time time;


}
