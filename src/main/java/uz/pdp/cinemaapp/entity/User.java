package uz.pdp.cinemaapp.entity;
//Sevinch Abdisattorova 03/14/2022 6:45 PM


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.cinemaapp.entity.enums.Gender;
import uz.pdp.cinemaapp.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "users")
@PackagePrivate
public class User extends AbsEntity {

    @Column(nullable = false, name = "full_name")
    String fullName;

    @Column(nullable = false, unique = true, length = 50)
    String username;

    String password;

    Date date_of_birth;

    @Enumerated(EnumType.STRING)
    Gender gender;

    @ManyToMany
    @JoinTable(
            name = "users_roles",
            joinColumns = { @JoinColumn(name = "user_id") },
            inverseJoinColumns = { @JoinColumn(name = "role_id")})
    List<Role> roles;

    @ManyToMany
    @JoinTable(
            name = "users_permissions",
            joinColumns = { @JoinColumn(name = "user_id") },
            inverseJoinColumns = { @JoinColumn(name = "permission_id")})
    List<Permission> permissions;
}
