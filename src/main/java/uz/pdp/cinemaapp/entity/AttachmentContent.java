package uz.pdp.cinemaapp.entity;
//Sevinch Abdisattorova 03/14/2022 6:45 PM


import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import uz.pdp.cinemaapp.entity.template.AbsEntity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@NoArgsConstructor
@Data
@Entity(name = "attachment_content")
@PackagePrivate
public class AttachmentContent extends AbsEntity {

    byte[] data;
    @OneToOne(cascade = CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    Attachment attachment;

    public AttachmentContent(byte[] data, Attachment attachment) {
        this.data = data;
        this.attachment = attachment;
    }
}
