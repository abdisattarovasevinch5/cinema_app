package uz.pdp.cinemaapp.entity;
//Sevinch Abdisattorova 03/14/2022 6:43 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import uz.pdp.cinemaapp.entity.template.AbsEntity;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "movie_sessions")
@PackagePrivate
public class MovieSession extends AbsEntity {

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    MovieAnnouncement movieAnnouncement;

    @ManyToOne
    @JoinColumn(name = "hall_id")
    Hall hall;


    @ManyToOne()
    @JoinColumn(name = "start_date_id")
    SessionDate startDate;

    @ManyToOne
    @JoinColumn(name = "start_time_id")
    SessionTime startTime;

    @ManyToOne
    @JoinColumn(name = "end_time_id")
    SessionTime endTime;


}
