package uz.pdp.cinemaapp.entity;
//Sevinch Abdisattorova 03/14/2022 6:45 PM


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.cinemaapp.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "rows")
@PackagePrivate
public class Row extends AbsEntity {
    Integer number;

    @ManyToOne
    Hall hall;

    @OneToMany(mappedBy = "row", cascade = CascadeType.ALL)
    List<Seat> seats;
}
