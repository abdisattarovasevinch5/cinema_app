package uz.pdp.cinemaapp.entity;
//Sevinch Abdisattorova 03/14/2022 6:58 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.cinemaapp.entity.enums.RoleEnum;
import uz.pdp.cinemaapp.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "roles")
@PackagePrivate
public class Role extends AbsEntity {

    Integer id;

    @Enumerated(EnumType.STRING)
    RoleEnum role;

    @ManyToMany
    @JoinTable(
            name = "roles_permissions",
            joinColumns = {@JoinColumn(name = "role_id")},
            inverseJoinColumns = {@JoinColumn(name = "permission_id")})
    List<Permission> permissions;
}
