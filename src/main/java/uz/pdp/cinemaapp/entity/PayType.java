package uz.pdp.cinemaapp.entity;
//Sevinch Abdisattorova 03/14/2022 6:45 PM


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.cinemaapp.entity.template.AbsEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "pay_types")
@PackagePrivate
public class PayType extends AbsEntity {

    String name;

    @Column(name = "commission_fee_in_percent")
    Integer commissionFeeInPercent;

}
