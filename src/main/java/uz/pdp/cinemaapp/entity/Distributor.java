package uz.pdp.cinemaapp.entity;
//Sevinch Abdisattorova 03/14/2022 6:34 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.cinemaapp.entity.template.AbsEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "distributors")
@PackagePrivate
public class Distributor extends AbsEntity {

    @Column(nullable = false)
    String name;

    String description;


}
