package uz.pdp.cinemaapp.entity;
//Sevinch Abdisattorova 03/14/2022 6:45 PM


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import uz.pdp.cinemaapp.entity.enums.MovieStatus;
import uz.pdp.cinemaapp.entity.template.AbsEntity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "movies")
@PackagePrivate
public class Movie extends AbsEntity {

    @Column(nullable = false, name = "title")
    String title;

    String description;

    @Column(name = "duration_in_minutes")
    Integer durationInMinutes;

    @OneToOne
    Attachment coverImage;

    @OneToOne
    Attachment trailerVideo;

    @ManyToMany
    @JoinTable(
            name = "movies_directors",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "director_id")})
    @OnDelete(action = OnDeleteAction.CASCADE)
    List<Director> directors;

    @ManyToMany
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinTable(
            name = "movies_actors",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "actor_id")})
    List<Actor> actors;

    @ManyToMany
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinTable(
            name = "movies_photos",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "photo_id")})
    List<Attachment> photos;

    @ManyToMany
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinTable(
            name = "movies_genres",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "genre_id")})
    List<Genre> genres;


    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    Distributor distributor;


    @Column(name = "distributor_share_in_percent")
    Integer distributorShareInPercent;


    @Column(name = "min_price")
    Double minPrice;

    @Enumerated(value = EnumType.STRING)
    MovieStatus status;

    LocalDate releaseDate;

    Double budget;


}
