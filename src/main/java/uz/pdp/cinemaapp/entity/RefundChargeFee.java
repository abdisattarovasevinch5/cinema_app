package uz.pdp.cinemaapp.entity;
//Sevinch Abdisattorova 03/14/2022 6:45 PM


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.cinemaapp.entity.template.AbsEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "refund_charge_fees")
@PackagePrivate
public class RefundChargeFee extends AbsEntity {

    @Column(name = "interval_in_minutes")
    Integer intervalInMinutes;

    Integer percentage;

}
