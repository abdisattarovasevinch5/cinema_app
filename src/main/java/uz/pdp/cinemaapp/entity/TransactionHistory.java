package uz.pdp.cinemaapp.entity;
//Sevinch Abdisattorova 03/14/2022 6:45 PM


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.cinemaapp.entity.enums.TicketStatus;
import uz.pdp.cinemaapp.entity.template.AbsEntity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "transaction_histories")
@PackagePrivate
public class TransactionHistory extends AbsEntity {

    @ManyToMany
    @JoinTable(
            name = "transaction_histories_tickets",
            joinColumns = {@JoinColumn(name = "transaction_history_id")},
            inverseJoinColumns = {@JoinColumn(name = "ticket_id")})
    List<Ticket> ticket;

//    LocalDate date;

    Double totalAmount;

    @OneToOne
    PayType payType;

    String paymentIntent;

    private boolean isRefunded;


}
