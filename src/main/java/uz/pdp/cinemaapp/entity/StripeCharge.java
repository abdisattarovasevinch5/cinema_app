package uz.pdp.cinemaapp.entity;
//Sevinch Abdisattorova 03/27/2022 11:29 PM

import lombok.Data;
import lombok.experimental.PackagePrivate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Data
@PackagePrivate
@Component
public class StripeCharge {
    Long amount;
    String receiptEmail;
    String source = "tok_visa";
    String currency = "usd";
    String description;

    public StripeCharge() {
    }

    public StripeCharge(Long amount, String receiptEmail, String source, String currency) {
        this.amount = amount;
        this.receiptEmail = receiptEmail;
        this.source = source;
        this.currency = currency;
    }


    public Map<String, Object> getCharge() {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("amount", this.amount);
        params.put("currency", this.currency);
        // source should obtained with Stripe.js
        params.put("source", this.source);
        params.put(
                "description",
                this.description
        );
        params.put("receipt_email", this.receiptEmail);
        return params;
    }
}
