package uz.pdp.cinemaapp.entity;
//Sevinch Abdisattorova 03/14/2022 6:43 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.cinemaapp.entity.template.AbsEntity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "halls")
@PackagePrivate
public class Hall extends AbsEntity {

    String name;
    @Column(name = "vip_additional_fee_in_percent")
    Integer vipAdditionalFeeInPercent;

    @OneToMany(mappedBy = "hall", cascade = CascadeType.ALL)
    List<Row> rows;
}
