package uz.pdp.cinemaapp.projection;
//Sevinch Abdisattorova 04/06/2022 1:42 PM


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CustomRowSeats {
    Integer rowNumber;
    Integer seatsCount;
    Integer priceCategoryId;
}
