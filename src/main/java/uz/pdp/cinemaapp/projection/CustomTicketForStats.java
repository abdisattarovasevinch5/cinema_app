package uz.pdp.cinemaapp.projection;


public interface CustomTicketForStats {

    Integer getTransactiondTicketsCount();

    Integer getRefundedTicketsCount();

    Integer getNewTicketsCount();
}
