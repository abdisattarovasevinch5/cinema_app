package uz.pdp.cinemaapp.projection;
//Sevinch Abdisattorova 03/28/2022 2:12 AM


import org.springframework.beans.factory.annotation.Value;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface CustomTransactionHistory {

    Integer getId();

    Integer getPayTypeId();

    String getPayTypeName();

    String getUserFullName();

    Integer getUserId();

    LocalDateTime getCreatedAt();

    Double getTotalAmount();

    Boolean getIsRefunded();

    String getPaymentIntent();


    @Value("#{@transactionHistoryRepository.getTicketsOfTransactionHistory(target.id)}")
    List<Integer> getTicketIds();


}
