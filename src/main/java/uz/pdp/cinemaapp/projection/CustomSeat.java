package uz.pdp.cinemaapp.projection;
//Sevinch Abdisattorova 03/25/2022 8:30 AM

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.cinemaapp.entity.Seat;

@Projection(types = Seat.class)
public interface CustomSeat {

    Integer getId();

    Integer getNumber();

    Integer getRow();

    String getHallName();

    Double getPrice();

    Boolean getIsAvailable();


}
