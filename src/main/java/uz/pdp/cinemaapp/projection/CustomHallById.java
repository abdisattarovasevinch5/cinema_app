package uz.pdp.cinemaapp.projection;
//Sevinch Abdisattorova 04/06/2022 1:59 PM


import org.springframework.beans.factory.annotation.Value;

import java.util.List;

public interface CustomHallById {
    Integer getHallId();

    String getHallName();

    Integer getVipFee();

    @Value("#{@rowRepository.findRowsAndSeatsCount(target.hallId)}")
    List<CustomRow> getRows();

}
