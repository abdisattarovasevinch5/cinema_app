package uz.pdp.cinemaapp.projection;
//Sevinch Abdisattorova 03/15/2022 8:58 PM


import org.springframework.data.rest.core.config.Projection;
import uz.pdp.cinemaapp.entity.Movie;

@Projection(types = Movie.class)
public interface CustomMovie {

    Integer getId();

    String getTitle();

    Integer getCoverImageId();
}
