package uz.pdp.cinemaapp.projection;
//Sevinch Abdisattorova 03/15/2022 8:58 PM


import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.cinemaapp.entity.Movie;

import java.time.LocalDate;
import java.util.List;

@Projection(types = Movie.class)
public interface CustomMovieById {

    Integer getId();

    String getTitle();

    Integer getDurationInMinutes();

    Integer getDistributorShareInPercent();

    Integer getMinPrice();

    LocalDate getReleaseDate();

    LocalDate getBudget();

    Integer getCoverImageId();

    Integer getTrailerVideoId();

    @Value("#{@distributorRepository.getDistributorsByMovieId(target.id)}")
    CustomDistributor getDistributor();


    @Value("#{@directorRepository.getDirectorsByMovieId(target.id)}")
    List<CustomDirector> getDirectors();


    @Value("#{@genreRepository.getGenresByMovieId(target.id)}")
    List<CustomGenre> getGenres();


    @Value("#{@actorRepository.getActorsByMovieId(target.id)}")
    List<CustomActor> getActors();


    @Value("#{@attachmentRepository.getAttachmentIdsByMovieId(target.id)}")
    List<Integer> getPhotos();


}
