package uz.pdp.cinemaapp.projection;
//Sevinch Abdisattorova 03/25/2022 8:30 AM

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.cinemaapp.entity.Ticket;
import uz.pdp.cinemaapp.entity.enums.TicketStatus;

@Projection(types = Ticket.class)
public interface CustomTicketForCart {

    Integer getId();

    TicketStatus getStatus();


}
