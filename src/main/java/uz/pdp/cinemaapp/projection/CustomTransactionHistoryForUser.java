package uz.pdp.cinemaapp.projection;
//Sevinch Abdisattorova 04/06/2022 4:02 PM


import org.springframework.beans.factory.annotation.Value;

import java.time.LocalDateTime;
import java.util.List;

public interface CustomTransactionHistoryForUser {
    Integer getId();

    Integer getPayTypeId();

    String getPayTypeName();

    LocalDateTime getCreatedAt();

    Double getTotalAmount();

    Boolean getIsRefunded();

    String getPaymentIntent();

    @Value("#{@transactionHistoryRepository.getTicketsOfTransactionHistory(target.id)}")
    List<Integer> getTicketIds();


}
