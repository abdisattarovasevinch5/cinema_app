package uz.pdp.cinemaapp.projection;
//Sevinch Abdisattorova 03/18/2022 11:07 AM


import org.springframework.beans.factory.annotation.Value;

import java.time.LocalDate;
import java.util.List;

public interface CustomMovieAnnouncement {

    Integer getId();

    Integer getMovieId();

    String getMovieTitle();

    Integer getMovieCoverImageId();

    Boolean getIsActive();

}
