package uz.pdp.cinemaapp.projection;
//Sevinch Abdisattorova 03/25/2022 8:30 AM

import java.time.LocalDate;
import java.time.LocalTime;

public interface CustomTicketForEmail {

    Integer getId();

    Integer getSeatNumber();

    Integer getRowNumber();

    String getHallName();

    Double getPrice();

    Integer getMovieSessionId();

    String getMovieName();

    LocalDate getStartDate();

    LocalTime getStartTime();

    byte[] getMovieCoverImg();

    byte[] getQrCode();

}
