package uz.pdp.cinemaapp.projection;
//Sevinch Abdisattorova 03/17/2022 11:01 AM


import org.springframework.data.rest.core.config.Projection;
import uz.pdp.cinemaapp.entity.Distributor;

@Projection(types = Distributor.class)
public interface CustomDistributor {
    Integer getId();

    String getName();

}
