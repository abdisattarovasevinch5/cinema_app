package uz.pdp.cinemaapp.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.cinemaapp.entity.Hall;

@Projection(types = Hall.class)
public interface CustomHallWithVIPFee {
    Integer getId();

    String getName();

    Integer getVipAdditionalFeeInPercent();
}
