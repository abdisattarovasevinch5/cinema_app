package uz.pdp.cinemaapp.projection;
//Sevinch Abdisattorova 03/17/2022 11:05 AM


import org.springframework.data.rest.core.config.Projection;
import uz.pdp.cinemaapp.entity.Genre;

@Projection(types = Genre.class)
public interface CustomGenre {

    Integer getId();

    String getName();

}
