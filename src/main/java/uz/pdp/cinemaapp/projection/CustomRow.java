package uz.pdp.cinemaapp.projection;
//Sevinch Abdisattorova 03/25/2022 8:30 AM

public interface CustomRow {

    Integer getRowId();

    Integer getRowNumber();

    Integer getSeatsCount();

}
