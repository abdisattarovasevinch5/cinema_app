package uz.pdp.cinemaapp.projection;
//Sevinch Abdisattorova 03/17/2022 11:32 AM


import org.springframework.data.rest.core.config.Projection;
import uz.pdp.cinemaapp.entity.Actor;

@Projection(types = Actor.class)
public interface CustomActor {

    Integer getId();

    String getFullName();
}
