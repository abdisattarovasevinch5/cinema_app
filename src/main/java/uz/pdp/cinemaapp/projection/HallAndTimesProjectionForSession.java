package uz.pdp.cinemaapp.projection;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.cinemaapp.entity.Hall;

import java.util.List;

//@Projection(types = Hall.class)
public interface HallAndTimesProjectionForSession {

    Integer getId();

    String getName();

    @Value("#{@sessionTimeRepository.getTimesByHallIdAndAnnouncementIdAndStartDateId(target.id, target.movieAnnouncementId, target.startDateId)}")
    List<SessionTimeProjection> getTimes();

}
