package uz.pdp.cinemaapp.projection;
//Sevinch Abdisattorova 03/17/2022 10:58 AM

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.cinemaapp.entity.Director;

@Projection(types = Director.class)
public interface CustomDirector {

    Integer getId();

    String getFullName();
}
