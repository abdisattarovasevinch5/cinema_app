package uz.pdp.cinemaapp.projection;
//Sevinch Abdisattorova 03/18/2022 11:40 AM


import org.springframework.data.rest.core.config.Projection;
import uz.pdp.cinemaapp.entity.SessionTime;

import java.time.LocalTime;
@Projection(types = SessionTime.class)
public interface SessionTimeProjection {

    Integer getId();

    LocalTime getTime();

    Integer getSessionId();


}
