package uz.pdp.cinemaapp.projection;
//Sevinch Abdisattorova 03/18/2022 11:07 AM


import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.cinemaapp.entity.MovieSession;

import java.time.LocalDate;
import java.util.List;

//@Projection(types = MovieSession.class)
public interface CustomMovieSession {

//    Integer getId();
    Integer getMovieAnnouncementId();

    Integer getMovieId();

    String getMovieTitle();

    Integer getMovieCoverImageId();

    Integer getStartDateId();

    LocalDate getStartDate();

    @Value("#{@hallRepository.getHallsAndTimesByMovieAnnouncementIdAndStartDateId(target.movieAnnouncementId,target.startDateId)}")
    List<HallAndTimesProjectionForSession> getHalls();



}
