package uz.pdp.cinemaapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.cinemaapp.entity.WaitingPurchaseTime;

@Repository
public interface WaitingTimeRepository extends JpaRepository<WaitingPurchaseTime, Integer> {

    @Query(nativeQuery = true, value = "select minute from waiting_purchase_time\n" +
            "order by created_at desc\n" +
            "limit 1")
    Integer getWaitingMinute();
}
