package uz.pdp.cinemaapp.repository;
//Sevinch Abdisattorova 03/14/2022 10:57 PM


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.cinemaapp.entity.Seat;
import uz.pdp.cinemaapp.projection.CustomSeat;

import java.util.List;

@Repository
public interface SeatRepository extends JpaRepository<Seat, Integer> {

    @Query(nativeQuery = true, value = "select distinct s.id,\n" +
            "                s.number,\n" +
            "                r.number     as row,\n" +
            "                h.name       as hallName,\n" +
            "                (coalesce(pc.additional_fee_in_percent, 0) * m2.min_price / 100) +\n" +
            "                (coalesce(h.vip_additional_fee_in_percent, 0) * m2.min_price / 100) +\n" +
            "                (coalesce((select percentage * m2.min_price / 100\n" +
            "                           from night_session_add_fees\n" +
            "                           where time <= st.time\n" +
            "                           order by h.created_at desc), 0)) +\n" +
            "                m2.min_price as price,\n" +
            "                (SELECT case\n" +
            "                            when\n" +
            "                                    s.id not in (\n" +
            "                                    select t.seat_id\n" +
            "                                    from tickets t\n" +
            "                                    where t.movie_session_id = :movieSessionId\n" +
            "                                      and t.status <> 'REFUNDED'\n" +
            "                                )\n" +
            "                                then true\n" +
            "                            else false\n" +
            "                            end\n" +
            "                )            as isAvailable\n" +
            "from seats s\n" +
            "         join rows r on r.id = s.row_id\n" +
            "         join halls h on r.hall_id = h.id\n" +
            "         join movie_sessions m on h.id = m.hall_id\n" +
            "         join session_times st on st.id = m.start_time_id\n" +
            "         join movie_announcements ma on ma.id = m.movie_announcement_id\n" +
            "         join movies m2 on m2.id = ma.movie_id\n" +
            "         join price_categories pc on s.price_category_id = pc.id\n" +
            "where m.id = :movieSessionId\n")
    List<CustomSeat> findAllAvailableSeatsOfMovieSession(Integer movieSessionId);


    @Query(nativeQuery = true, value =
            " select distinct " +
                    "(coalesce(pc.additional_fee_in_percent, 0) * m2.min_price / 100) +\n" +
                    "                (coalesce(h.vip_additional_fee_in_percent, 0) * m2.min_price / 100) +\n" +
                    "                (coalesce((select percentage * m2.min_price / 100\n" +
                    "                           from night_session_add_fees\n" +
                    "                           where time <= st.time\n" +
                    "                           order by h.created_at desc), 0)) +\n" +
                    "                m2.min_price as price\n" +
                    "from seats s\n" +
                    "         join rows r on r.id = s.row_id\n" +
                    "         join halls h on r.hall_id = h.id\n" +
                    "         join movie_sessions m on h.id = m.hall_id\n" +
                    "         join session_times st on st.id = m.start_time_id\n" +
                    "         join movie_announcements ma on ma.id = m.movie_announcement_id\n" +
                    "         join movies m2 on m2.id = ma.movie_id\n" +
                    "         join price_categories pc on s.price_category_id = pc.id\n" +
                    "where m.id = :movieSessionId\n" +
                    "  and s.id = :seatId")
    Double getPriceOfSeatBySeatIdAndMovieSessionId(Integer seatId, Integer movieSessionId);
}
