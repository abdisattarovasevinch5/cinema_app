package uz.pdp.cinemaapp.repository;
//Sevinch Abdisattorova 03/25/2022 10:14 AM


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.cinemaapp.entity.User;
import uz.pdp.cinemaapp.projection.CustomTicket;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

}
