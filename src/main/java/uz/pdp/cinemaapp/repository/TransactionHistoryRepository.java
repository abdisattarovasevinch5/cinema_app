package uz.pdp.cinemaapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.cinemaapp.entity.TransactionHistory;
import uz.pdp.cinemaapp.projection.CustomTransactionHistory;
import uz.pdp.cinemaapp.projection.CustomTransactionHistoryForUser;

import java.util.List;

@Repository
public interface TransactionHistoryRepository extends JpaRepository<TransactionHistory, Integer> {

    @Query(nativeQuery = true, value = "select th.id                   as id,\n" +
            "       pt.id                   as payTypeId,\n" +
            "       pt.name                 as payTypeName,\n" +
            "       th.created_at           as createdAt,\n" +
            "       th.payment_intent       as paymentIntent,\n" +
            "       th.total_amount         as totalAmount,\n" +
            "       (select th.is_refunded) as isRefunded,\n" +
            "       u.id                    as userId,\n" +
            "       u.full_name             as userFullName\n" +
            "from transaction_histories th\n" +
            "         join transaction_histories_tickets tht on th.id = tht.transaction_history_id\n" +
            "         join tickets t on t.id = tht.ticket_id\n" +
            "         join users u on u.id = t.user_id\n" +
            "         join pay_types pt on pt.id = th.pay_type_id\n" +
            "group by th.id, pt.id, pt.name, th.created_at, th.payment_intent, th.total_amount, (select th.is_refunded), u.id,\n" +
            "         u.full_name")
    List<CustomTransactionHistory> getAllTransactionHistories();

    @Query(nativeQuery = true, value = "select th.id                   as id,\n" +
            "       pt.id                   as payTypeId,\n" +
            "       pt.name                 as payTypeName,\n" +
            "       th.created_at           as createdAt,\n" +
            "       th.payment_intent       as paymentIntent,\n" +
            "       th.total_amount         as totalAmount,\n" +
            "       (select th.is_refunded) as isRefunded\n" +
            "from transaction_histories th\n" +
            "         join transaction_histories_tickets tht on th.id = tht.transaction_history_id\n" +
            "         join tickets t on t.id = tht.ticket_id\n" +
            "         join users u on u.id = t.user_id\n" +
            "         join pay_types pt on pt.id = th.pay_type_id\n" +
            "where user_id = :userId\n" +
            "group by th.id, pt.id, pt.name, th.created_at, th.payment_intent, th.total_amount, (select th.is_refunded)")
    List<CustomTransactionHistoryForUser> getAllTransactionHistoriesOfUser(Integer userId);


    @Query(nativeQuery = true, value = "select th.payment_intent\n" +
            "from transaction_histories th\n" +
            "         join transaction_histories_tickets tht on th.id = tht.transaction_history_id\n" +
            "         join tickets t on t.id = tht.ticket_id\n" +
            "where ticket_id =:ticketId ")
    String getPaymentIntentByTicketId(Integer ticketId);

    @Query(nativeQuery = true, value = "select t.id\n" +
            "from transaction_histories th\n" +
            "         join transaction_histories_tickets tht on th.id = tht.transaction_history_id\n" +
            "         join tickets t on t.id = tht.ticket_id\n" +
            "where th.id = :id")
    List<Integer> getTicketsOfTransactionHistory(Integer id);
}
