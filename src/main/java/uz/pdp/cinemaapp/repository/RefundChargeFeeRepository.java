package uz.pdp.cinemaapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.cinemaapp.entity.RefundChargeFee;

@Repository
public interface RefundChargeFeeRepository extends JpaRepository<RefundChargeFee, Integer> {

    RefundChargeFee findRefundChargeFeeByIntervalInMinutesGreaterThanEqualOrderByIntervalInMinutesDesc(Integer intervalInMinutes);
}
