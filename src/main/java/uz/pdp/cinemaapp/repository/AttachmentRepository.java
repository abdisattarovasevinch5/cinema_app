package uz.pdp.cinemaapp.repository;
//Sevinch Abdisattorova 03/14/2022 10:57 PM


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.cinemaapp.entity.Actor;
import uz.pdp.cinemaapp.entity.Attachment;

import java.util.List;

@Repository
public interface AttachmentRepository extends JpaRepository<Attachment, Integer> {

    @Query(nativeQuery = true, value = "select a.id" +
            " from movies_photos" +
            " join attachments a on a.id = movies_photos.photo_id" +
            " where movie_id = :movieId")
    List<Integer> getAttachmentIdsByMovieId(Integer movieId);
}
