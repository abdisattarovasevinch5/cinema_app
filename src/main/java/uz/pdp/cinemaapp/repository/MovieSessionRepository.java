package uz.pdp.cinemaapp.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.cinemaapp.entity.MovieSession;
import uz.pdp.cinemaapp.projection.CustomMovieSession;

public interface MovieSessionRepository extends JpaRepository<MovieSession, Integer> {


    @Query(nativeQuery = true, value = "select distinct" +
            "     ma.id as movieAnnouncementId,\n" +
            "     ma.movie_id as movieId,\n" +
            "     m.title  as movieTitle,\n" +
            "     m.cover_image_id  as movieCoverImgId,\n" +
            "     sd.id    as startDateId, \n" +
            "     sd.date   as startDate\n" +
            "from movie_sessions ms\n" +
            "     join movie_announcements ma on ms.movie_announcement_id = ma.id " +
            "     join movies m on m.id = ma.movie_id\n" +
            "     join session_dates sd on ms.start_date_id = sd.id\n" +
            "where lower(m.title) like lower(concat('%',:search,'%')) and " +
            "sd.date >= cast(now() as date)\n" +
            "order by sd.date")
    Page<CustomMovieSession> findAllSessionsByPage(Pageable pageable, String search);

}
