package uz.pdp.cinemaapp.repository;
//Sevinch Abdisattorova 04/05/2022 9:31 PM


import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.cinemaapp.entity.CashBox;

public interface CashBoxRepository extends JpaRepository<CashBox, Integer> {

}
