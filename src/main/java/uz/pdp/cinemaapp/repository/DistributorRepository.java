package uz.pdp.cinemaapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.cinemaapp.entity.Distributor;
import uz.pdp.cinemaapp.projection.CustomDistributor;

//@RepositoryRestResource(path = "distributors")
@Repository
public interface DistributorRepository extends JpaRepository<Distributor, Integer> {


    @Query(nativeQuery = true, value = "select d.id as id, d.name as name" +
            " from movies" +
            " join distributors d on movies.distributor_id = d.id\n" +
            "where movies.id = :movieId")
    CustomDistributor getDistributorsByMovieId(Integer movieId);

}
