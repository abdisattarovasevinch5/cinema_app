package uz.pdp.cinemaapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.cinemaapp.entity.NightSessionAddFee;

@Repository
public interface NightSessionFeeRepository extends JpaRepository<NightSessionAddFee, Integer> {
}
