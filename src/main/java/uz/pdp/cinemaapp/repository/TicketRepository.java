package uz.pdp.cinemaapp.repository;
//Sevinch Abdisattorova 03/25/2022 9:22 AM


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.cinemaapp.entity.Ticket;
import uz.pdp.cinemaapp.entity.enums.TicketStatus;
import uz.pdp.cinemaapp.projection.CustomTicket;
import uz.pdp.cinemaapp.projection.CustomTicketForCart;
import uz.pdp.cinemaapp.projection.CustomTicketForEmail;
import uz.pdp.cinemaapp.projection.CustomTicketForStats;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Integer> {

    @Query(nativeQuery = true, value = "select distinct t.id,\n" +
            "                s.number     as seatNumber,\n" +
            "                r.number     as rowNumber,\n" +
            "                h.name       as hallName,\n" +
            "                (coalesce(pc.additional_fee_in_percent, 0) * m.min_price / 100) +\n" +
            "                (coalesce(h.vip_additional_fee_in_percent, 0) * m.min_price / 100) +\n" +
            "                (coalesce((select percentage * m.min_price / 100\n" +
            "                           from night_session_add_fees\n" +
            "                           where time <= st.time\n" +
            "                           order by h.created_at desc), 0)) +\n" +
            "                m.min_price as price,\n" +
            "                ms.id        as movieSessionId,\n" +
            "                m.title      as movieName,\n" +
            "                sd.date      as startDate,\n" +
            "                st.time      as startTime\n" +
            "\n" +
            "from tickets t\n" +
            "         join seats s on s.id = t.seat_id\n" +
            "         join rows r on r.id = s.row_id\n" +
            "         join halls h on h.id = r.hall_id\n" +
            "         join movie_sessions ms on ms.id = t.movie_session_id\n" +
            "         join movie_announcements ma on ma.id = ms.movie_announcement_id\n" +
            "         join movies m on m.id = ma.movie_id\n" +
            "         join price_categories pc on pc.id = s.price_category_id\n" +
            "         join session_dates sd on sd.id = ms.start_date_id\n" +
            "         join session_times st on st.id = ms.start_time_id\n" +
            "where t.status = 'NEW'\n" +
            "  and t.user_id = :userId")
    List<CustomTicket> getTicketsInTheCart(Integer userId);

    @Query(nativeQuery = true, value = "select distinct t.id,\n" +
            "                s.number    as seatNumber,\n" +
            "                r.number    as rowNumber,\n" +
            "                h.name      as hallName,\n" +
            "                (coalesce(pc.additional_fee_in_percent, 0) * m.min_price / 100) +\n" +
            "                (coalesce(h.vip_additional_fee_in_percent, 0) * m.min_price / 100) +\n" +
            "                (coalesce((select percentage * m.min_price / 100\n" +
            "                           from night_session_add_fees\n" +
            "                           where time <= st.time\n" +
            "                           order by h.created_at desc), 0)) +\n" +
            "                m.min_price as price,\n" +
            "                ms.id       as movieSessionId,\n" +
            "                m.title     as movieName,\n" +
            "                sd.date     as startDate,\n" +
            "                st.time     as startTime,\n" +
            "                ac.data     as qrCode,\n" +
            "                c.data      as movieCoverImg\n" +
            "\n" +
            "from tickets t\n" +
            "         join seats s on s.id = t.seat_id\n" +
            "         join rows r on r.id = s.row_id\n" +
            "         join halls h on h.id = r.hall_id\n" +
            "         join movie_sessions ms on ms.id = t.movie_session_id\n" +
            "         join movie_announcements ma on ma.id = ms.movie_announcement_id\n" +
            "         join movies m on m.id = ma.movie_id\n" +
            "         join price_categories pc on pc.id = s.price_category_id\n" +
            "         join session_dates sd on sd.id = ms.start_date_id\n" +
            "         join session_times st on st.id = ms.start_time_id\n" +
            "         join attachments a on a.id = t.qr_code_id\n" +
            "         join attachment_content ac on a.id = ac.attachment_id\n" +
            "         join attachments a2 on a2.id = m.cover_image_id\n" +
            "         join attachment_content c on a2.id = c.attachment_id\n" +
            "where t.status = 'NEW'\n" +
            "  and t.user_id = :userId")
    List<CustomTicketForEmail> getTicketsInTheCartForPurchaseEmail(Integer userId);

    @Query(nativeQuery = true, value = "delete from tickets\n" +
            "where user_id = :userId\n" +
            "and status = 'NEW'")
    void clearCartOfUser(Integer userId);


    @Query(nativeQuery = true, value = "delete\n" +
            "from tickets\n" +
            "where status = 'NEW'\n" +
            "  and user_id = :userId\n" +
            "  and id = :ticketId")
    void deleteTicketFromCart(Integer userId, Integer ticketId);


    @Query(nativeQuery = true, value = "select id, status from tickets\n" +
            "where id=:id")
    CustomTicketForCart getTicketByIdForCart(Integer id);


    @Query(nativeQuery = true, value = "select distinct t.id,t.status from  tickets t\n" +
            "where movie_session_id =:movieSessionId\n" +
            "and seat_id =:seatId\n" +
            "and status<>'REFUNDED'\n")
    CustomTicketForCart checkIfSeatIsAvailable(Integer seatId, Integer movieSessionId);


    List<Ticket> findByUserIdAndStatus(Integer userid, TicketStatus status);


    @Query(nativeQuery = true, value = "select distinct t.id        as id,\n" +
            "                (coalesce(pc.additional_fee_in_percent, 0) * m.min_price / 100) +\n" +
            "                (coalesce(h.vip_additional_fee_in_percent, 0) * m.min_price / 100) +\n" +
            "                (coalesce((select percentage * m.min_price / 100\n" +
            "                           from night_session_add_fees\n" +
            "                           where time <= st.time\n" +
            "                           order by h.created_at desc), 0)) +\n" +
            "                m.min_price as price\n" +
            "from tickets t\n" +
            "         join movie_sessions ms on ms.id = t.movie_session_id\n" +
            "         join movie_announcements ma on ma.id = ms.movie_announcement_id\n" +
            "         join movies m on m.id = ma.movie_id\n" +
            "         join seats s on s.id = t.seat_id\n" +
            "         join price_categories pc on pc.id = s.price_category_id\n" +
            "         join halls h on h.id = ms.hall_id\n" +
            "         join session_dates sd on sd.id = ms.start_date_id\n" +
            "         join session_times st on ms.start_time_id = st.id\n" +
            "where t.id = :ticketId")
    CustomTicket findTicketForRefund(Integer ticketId);


    @Query(nativeQuery = true, value = "update tickets t\n" +
            "set status = 'REFUNDED'\n" +
            "where id = :ticketId")
    void changeTicketStatusToRefunded(Integer ticketId);


    @Query(nativeQuery = true, value = " select cast((sd.date + st.time) as timestamp)  " +
            "               from tickets t " +
            "                       join movie_sessions ms on ms.id = t.movie_session_id " +
            "                         join session_dates sd on sd.id = ms.start_date_id " +
            "                         join session_times st on st.id = ms.start_time_id " +
            "                where t.id = :ticketId")
    Timestamp getStartTimeOfTicket(Integer ticketId);

    @Query(nativeQuery = true, value =
            "select percentage " +
                    "from refund_charge_fees rf " +
                    "where extract(epoch from ( :sessionStartTime - now()) / 60) >= rf.interval_in_minutes " +
                    "order by interval_in_minutes desc  " +
                    "limit 1")
    Integer getRefundChargeFeePercentByTicketId(Timestamp sessionStartTime);


    @Query(nativeQuery = true, value = "select (select count(*)\n" +
            "        from tickets\n" +
            "        where status = 'NEW' " +
            "          and created_at::date = :date) as newTicketsCount,\n" +
            "       (select count(*)\n" +
            "        from tickets\n" +
            "        where status = 'PURCHASED'\n" +
            "          and created_at::date = :date) as purchasedTicketsCount,\n" +
            "       (select count(*)\n" +
            "        from tickets\n" +
            "        where status = 'REFUNDED'\n" +
            "          and created_at::date = :date) as refundedTicketsCount\n")
    CustomTicketForStats getTicketsOfDate(LocalDate date);


    @Query(nativeQuery = true, value = "" +
            "select (select count(*)\n" +
            "        from tickets\n" +
            "        where status = 'NEW'\n" +
            "          and extract(month from created_at) = :month\n" +
            "          and extract(year from created_at) = :year) as newTicketsCount,\n" +
            "       (select count(*)\n" +
            "        from tickets\n" +
            "        where status = 'PURCHASED'\n" +
            "          and extract(month from created_at) = :month\n" +
            "          and extract(year from created_at) = :year) as purchasedTicketsCount,\n" +
            "       (select count(*)\n" +
            "        from tickets\n" +
            "        where status = 'REFUNDED'\n" +
            "          and extract(month from created_at) = :month\n" +
            "          and extract(year from created_at) = :year) as refundedTicketsCount\n")
    CustomTicketForStats getTicketsOfMonth(Integer month, Integer year);


    @Query(nativeQuery = true, value = "select count(*)\n" +
            "from movie_sessions ms\n" +
            "         join session_dates sd on sd.id = ms.start_date_id\n" +
            "where sd.date = :date")
    Integer countSessionsOfDate(LocalDate date);


    @Query(nativeQuery = true, value = "select count(*)\n" +
            "from movie_sessions ms\n" +
            "         join session_dates sd on sd.id = ms.start_date_id\n" +
            "where extract(month from sd.date) = :month\n" +
            "  and extract(year from sd.date) = :year")
    Integer countSessionsOfMonth(Integer month, Integer year);
}
