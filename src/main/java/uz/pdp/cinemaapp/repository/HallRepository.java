package uz.pdp.cinemaapp.repository;
//Sevinch Abdisattorova 03/14/2022 10:57 PM


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.cinemaapp.entity.Hall;
import uz.pdp.cinemaapp.projection.CustomHallById;
import uz.pdp.cinemaapp.projection.CustomHallWithVIPFee;
import uz.pdp.cinemaapp.projection.HallAndTimesProjectionForSession;

import java.util.List;

@Repository
public interface HallRepository extends JpaRepository<Hall, Integer> {

//    @Query(nativeQuery = true, value = "select distinct h.id   as id, " +
//            "h.name as name " +
//            "from movie_sessions " +
//            "join halls h on h.id = movie_sessions.hall_id " +
//            "where movie_announcement_id=:afishaId")
//    List<CustomHall> getHallsOfAfisha(Integer afishaId);


    @Query(nativeQuery = true, value = "select id   as id," +
            " name as name," +
            " vip_additional_fee_in_percent" +
            " as vipAdditionalFeeInPercent" +
            " from halls")
    List<CustomHallWithVIPFee> getAllHalls();

    @Query(nativeQuery = true, value = "select distinct" +
            " h.id  as id, " +
            "       h.name,  " +
            " rh.start_date_id  as startDateId, " +
            " movie_announcement_id  as movieAnnouncementId " +
            "from halls h " +
            "join movie_sessions rh on h.id = rh.hall_id " +
            "where rh.start_date_id = :startDateId " +
            "and movie_announcement_id = :movieAnnouncementId")
    List<HallAndTimesProjectionForSession> getHallsAndTimesByMovieAnnouncementIdAndStartDateId(
            Integer movieAnnouncementId, Integer startDateId);

    @Query(nativeQuery = true, value = "select distinct h.id                            as hallId,\n" +
            "                h.name                          as hallName,\n" +
            "                h.vip_additional_fee_in_percent as vipFee\n" +
            "from halls h\n" +
            "where h.id = :id")
    CustomHallById findHallById(Integer id);
}
