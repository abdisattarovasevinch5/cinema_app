package uz.pdp.cinemaapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.cinemaapp.entity.Genre;
import uz.pdp.cinemaapp.projection.CustomGenre;

import java.util.List;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Integer> {

    @Query(value = "delete " +
            " from movies_genres " +
            " where movie_id = :movieId", nativeQuery = true)
    void deleteGenresOfMovie(Integer movieId);


    @Query(nativeQuery = true, value = " select g.id as id,g.name as name" +
            " from movies_genres" +
            " join genres g on g.id = movies_genres.genre_id" +
            " where movie_id = :movieId")
    List<CustomGenre> getGenresByMovieId(Integer movieId);
}
