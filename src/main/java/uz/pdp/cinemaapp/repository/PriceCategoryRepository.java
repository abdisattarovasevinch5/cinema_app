package uz.pdp.cinemaapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.cinemaapp.entity.PriceCategory;

@Repository
public interface PriceCategoryRepository extends JpaRepository<PriceCategory, Integer> {
}
