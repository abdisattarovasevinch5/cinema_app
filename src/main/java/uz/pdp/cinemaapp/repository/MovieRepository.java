package uz.pdp.cinemaapp.repository;
//Sevinch Abdisattorova 03/14/2022 10:57 PM


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.cinemaapp.entity.Movie;
import uz.pdp.cinemaapp.projection.CustomMovie;
import uz.pdp.cinemaapp.projection.CustomMovieById;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> {

    @Query(value = "select m.id, m.title as title, a.id as coverImageId" +
            " from movies m " +
            " join attachments a on a.id = m.cover_image_id" +
            " where lower (m.title) like lower (concat('%',:search ,'%'))", nativeQuery = true)
    Page<CustomMovie> getAllMovies(Pageable pageable, String search);

    @Query(value = "delete " +
            " from movies_actors " +
            " where movie_id = :movieId", nativeQuery = true)
    void deleteActorsOfMovie(Integer movieId);


    @Query(value = "delete " +
            " from movies_photos " +
            " where movie_id = :movieId", nativeQuery = true)
    void deletePhotosOfMovie(Integer movieId);

    @Query(nativeQuery = true,value = "select m.id,\n" +
            "       title,\n" +
            "       description,\n" +
            "       duration_in_minutes          as durationInMinutes,\n" +
            "       distributor_share_in_percent as distributorShareInPercent,\n" +
            "       min_price                    as minPrice,\n" +
            "       a.id as coverImageId,\n" +
            "       a2.id as trailerVideoId\n" +
            "from movies m\n" +
            "         join attachments a on a.id = m.cover_image_id\n" +
            "         join attachments a2 on a2.id = m.trailer_video_id\n" +
            "where m.id =:movieId ")
    CustomMovieById getMovieById (Integer movieId);
}
