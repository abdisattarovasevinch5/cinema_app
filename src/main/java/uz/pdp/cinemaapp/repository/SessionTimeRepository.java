package uz.pdp.cinemaapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.cinemaapp.entity.SessionTime;
import uz.pdp.cinemaapp.projection.SessionTimeProjection;

import java.sql.Time;
import java.util.List;

public interface SessionTimeRepository extends JpaRepository<SessionTime, Integer> {

    SessionTime findDistinctFirstByTime(Time time);

    @Query(value = "select distinct " +
            "st.id as id," +
            "ms.id  as sessionId," +
            "time " +
            "from session_times st\n" +
            "join movie_sessions ms on st.id = ms.start_time_id\n" +
            "where ms.hall_id = :hallId\n" +
            "and movie_announcement_id = :movieAnnouncementId " +
            "and ms.start_date_id = :startDateId", nativeQuery = true)
    List<SessionTimeProjection> getTimesByHallIdAndAnnouncementIdAndStartDateId(Integer hallId,Integer movieAnnouncementId,Integer startDateId);
}
