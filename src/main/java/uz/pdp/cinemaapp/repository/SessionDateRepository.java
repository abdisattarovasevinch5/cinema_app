package uz.pdp.cinemaapp.repository;
//Sevinch Abdisattorova 03/17/2022 4:40 PM


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.cinemaapp.entity.SessionDate;

import java.time.LocalDate;

@Repository
public interface SessionDateRepository extends JpaRepository<SessionDate, Integer> {

    SessionDate findDistinctFirstByDate(LocalDate date);
}
