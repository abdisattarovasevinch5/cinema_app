package uz.pdp.cinemaapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.cinemaapp.entity.PayType;

@Repository
public interface PaymentTypeRepository extends JpaRepository<PayType, Integer> {
}
