package uz.pdp.cinemaapp.repository;
//Sevinch Abdisattorova 03/17/2022 3:07 PM


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.cinemaapp.entity.MovieAnnouncement;
import uz.pdp.cinemaapp.projection.CustomMovieAnnouncement;

import java.util.List;

public interface MovieAnnouncementRepository extends JpaRepository<MovieAnnouncement, Integer> {


    @Query(nativeQuery = true, value = "select ma.id            as id,\n" +
            "       m.id             as movieId,\n" +
            "       m.title          as movieTitle,\n" +
            "       m.cover_image_id as movieCoverImageId,\n" +
            "       ma.is_active     as isActive\n" +
            "from movie_announcements ma\n" +
            "         join movies m on m.id = ma.movie_id\n")
    List<CustomMovieAnnouncement> getAllMovieAnnouncements();


    @Query(nativeQuery = true, value = "select ma.id            as id,\n" +
            "       m.id             as movieId,\n" +
            "       m.title          as movieTitle,\n" +
            "       m.cover_image_id as movieCoverImageId,\n" +
            "       ma.is_active     as isActive\n" +
            "from movie_announcements ma\n" +
            "         join movies m on m.id = ma.movie_id" +
            " where ma.id= :movieAnnouncementId")
    CustomMovieAnnouncement getMovieAnnouncementById(Integer movieAnnouncementId);

    MovieAnnouncement findMovieAnnouncementByMovieId(Integer movie_id);



   /* @Query(nativeQuery = true, value = "select a.id  as id," +
            "       m.id  as movieId," +
            "       m.title  as movieTitle," +
            "       m.cover_image_id as movieCoverImageId," +
            "       sd.date  as startDate" +
            " from movie_announcements a" +
            "         join movies m on m.id = a.movie_id" +
            "         join movie_sessions rh on a.id = rh.movie_announcement_id" +
            "         join session_dates sd on rh.start_date_id = sd.id" +
            " where sd.date > date(now()) and" +
            "      lower(title) like lower(concat('%',:search,'%'))")
    List<CustomMovieAnnouncement> getAllMovieAnnouncements(Pageable pageable, String search);*/
}
