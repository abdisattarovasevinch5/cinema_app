package uz.pdp.cinemaapp.repository;
//Sevinch Abdisattorova 03/14/2022 10:57 PM


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.cinemaapp.entity.Actor;
import uz.pdp.cinemaapp.entity.Row;
import uz.pdp.cinemaapp.projection.CustomRow;

import java.util.List;

@Repository
public interface RowRepository extends JpaRepository<Row, Integer> {


    @Query(nativeQuery = true, value = "select distinct r.id                                                   as rowId,\n" +
            "                r.number                                               as rowNumber,\n" +
            "                (select count(*) from seats where seats.row_id = r.id) as seatsCount\n" +
            "from halls h\n" +
            "         join rows r on h.id = r.hall_id\n" +
            "         join seats s on s.row_id = r.id\n" +
            "where h.id = :hallId ")
    List<CustomRow> findRowsAndSeatsCount(Integer hallId);
}
