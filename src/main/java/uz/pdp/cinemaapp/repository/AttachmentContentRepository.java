package uz.pdp.cinemaapp.repository;
//Sevinch Abdisattorova 03/14/2022 10:57 PM


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.cinemaapp.entity.Attachment;
import uz.pdp.cinemaapp.entity.AttachmentContent;

@Repository
public interface AttachmentContentRepository extends JpaRepository<AttachmentContent, Integer> {

    @Query(nativeQuery = true, value = "select data\n" +
            "from attachment_content\n" +
            "         join attachments a on a.id = attachment_content.attachment_id\n" +
            "where a.id = :attachment_id ")
    byte[] findAttachmentContentByAttachmentId(Integer attachment_id);


    AttachmentContent findAttachmentContentByAttachment_Id(Integer attachment_id);
}
