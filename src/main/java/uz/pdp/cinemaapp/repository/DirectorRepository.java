package uz.pdp.cinemaapp.repository;
//Sevinch Abdisattorova 03/14/2022 10:57 PM


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.cinemaapp.entity.Director;
import uz.pdp.cinemaapp.projection.CustomDirector;

import java.util.List;

@Repository
public interface DirectorRepository extends JpaRepository<Director, Integer> {
    @Query(value = "delete " +
            " from movies_directors " +
            " where movie_id = :movieId", nativeQuery = true)
    void deleteDirectorsOfMovie(Integer movieId);

    @Query(nativeQuery = true, value = "select d.id as id, d.full_name as fullName " +
            "from movies_directors " +
            "join directors d on d.id = movies_directors.director_id" +
            " where movie_id = :movieId")
    List<CustomDirector> getDirectorsByMovieId(Integer movieId);
}
