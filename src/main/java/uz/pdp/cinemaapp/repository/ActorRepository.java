package uz.pdp.cinemaapp.repository;
//Sevinch Abdisattorova 03/14/2022 10:57 PM


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.cinemaapp.entity.Actor;
import uz.pdp.cinemaapp.projection.CustomActor;

import java.util.List;

@Repository
public interface ActorRepository extends JpaRepository<Actor, Integer> {

    @Query(nativeQuery = true, value = "select a.id as id,a.full_name as fullName from movies_actors\n" +
            "join actors a on a.id = movies_actors.actor_id\n" +
            "where movie_id=:movieId")
    List<CustomActor> getActorsByMovieId(Integer movieId);
}
