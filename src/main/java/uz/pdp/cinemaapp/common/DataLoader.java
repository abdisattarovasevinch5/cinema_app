package uz.pdp.cinemaapp.common;
//Sevinch Abdisattorova 03/15/2022 9:31 AM


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import uz.pdp.cinemaapp.entity.Hall;
import uz.pdp.cinemaapp.entity.Row;
import uz.pdp.cinemaapp.repository.*;

import java.util.ArrayList;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Component
public class DataLoader implements CommandLineRunner {

    HallRepository hallRepository;

    @Autowired
    public DataLoader(HallRepository hallRepository) {
        this.hallRepository = hallRepository;
    }

    @Value("${spring.sql.init.mode}")
    String initMode;


    @Override
    public void run(String... args) throws Exception {
        if (initMode.equals("always")) {
            Hall hall = new Hall();
            hall.setName("Hall 1");
            ArrayList<Row> rows = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                rows.add(new Row(i + 1, hall, null));
            }

            hall.setRows(rows);
            hallRepository.save(hall);
        }

    }
}
